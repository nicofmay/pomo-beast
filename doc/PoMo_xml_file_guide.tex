

\documentclass[10pt,letterpaper]{article}
\usepackage[top=0.85in,left=2.75in,footskip=0.75in]{geometry}
\usepackage{changepage}
\usepackage[utf8]{inputenc}
\usepackage{textcomp,marvosym}
\usepackage{fixltx2e}
\usepackage{amsmath,amssymb}
\usepackage{cite}
\usepackage{nameref,hyperref}
\usepackage[right]{lineno}
\usepackage{listings}
\usepackage{microtype}
\DisableLigatures[f]{encoding = *, family = * }
\usepackage{rotating}
\usepackage{bm}
\usepackage{color}
\raggedright
\setlength{\parindent}{0.5cm}
\textwidth 5.25in 
\textheight 8.75in
\usepackage[aboveskip=1pt,labelfont=bf,labelsep=period,justification=raggedright,singlelinecheck=off]{caption}

\makeatletter
\renewcommand{\@biblabel}[1]{\quad#1.}
\makeatother
\date{}

\begin{document}
\vspace*{0.35in}



{\Huge
\textbf\newline{How to write/edit an xml file for PoMo}
}
\newline

{\Large
\textbf\newline{Nicola De Maio}
}
\newline
{
\textbf\newline{\today}
}
\newline

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}






\section{Installing PoMo}

PoMo is a BEAST2 package. 
To install it, first download and install BEAST2 if you have not already done so (\emph{http://beast2.org/}). earliest versions compatible are BEAST 2.2 and 2.3.
Then, from the BEAST2 distribution, run BEAUti (double-click on the BEAUti icon contained in the BEAST2 folder).
In BEAUti, click on "File" from the top menu, select "Manage packages" from the scroll-down menu, then select PoMo from the table of packages, and click on "Install/Upgrade".
Now BEAST2 will run PoMo xml files.

The source code of PoMo, examples, and this documentation, can be also found in  \emph{https://bitbucket.org/nicofmay/pomo-beast/} which also includes an alternative installation procedure.




\section{Creating a working xml file}

it is possible to create a working xml input file for PoMo by using one of the given example files as template, and manually adding/modifying parts as explained below. 
In the following, I will assume that the "PoMo5.xml" example file has been chosen as a template.




\subsection{Including the genetic alignment}

An example genetic alignment section:

\begin{lstlisting}
<alignmentPoMo spec="alignment.pomo.evolution.AlignmentPoMo" id="alignment1" dataTypePoMo="PoModata">
	<sequence spec="SequencePoMo" id="seq1" taxon="t1" value="0-1-0-9,0-0-0-10,10-0-0-0,0-0-0-10,0-10-0-0,10-0-0-0,0-0-10-0,0-0-0-10,0-0-0-10,10-0-0-0,0-0-0-10,0-10-0-0,0-10-0-0,0-0-10-0,0-0-0-10,0-0-10-0,10-0-0-0,0-0-0-10,0-10-0-0,0-0-0-10,10-0-0-0,0-0-10-0,0-10-0-0,10-0-0-0"/>
	<sequence spec="SequencePoMo" id="seq2" taxon="t2" value="0-1-0-9,0-0-0-10,10-0-0-0,0-0-0-10,0-10-0-0,10-0-0-0,0-0-10-0,0-0-0-10,0-0-0-10,10-0-0-0,0-0-0-10,0-10-0-0,0-10-0-0,0-0-10-0,0-0-0-10,0-0-10-0,10-0-0-0,0-0-0-10,0-10-0-0,0-0-0-10,10-0-0-0,0-0-10-0,0-10-0-0,10-0-0-0"/>
</alignmentPoMo>
\end{lstlisting}

Data for each site and population is in the form "0-1-0-9", that is, 4 dash-separated integers. These four integers represent respectively the allele counts of As, Cs, Gs, and Ts at the given position and population.
It is important that all taxa have the same number of positions. An element "0-0-0-0" is equivalent to providing no data (similar to an N for nucleotide alignments).



\subsection{Including sampling times}

As in all BEAST2 applications, sampling times can be specified with:

\begin{lstlisting}
      <timeTraitSet spec='beast.evolution.tree.TraitSet' id='timeTraitSet' traitname="date-forward" value="t1=2015.0,t2=2015.1">   
 <taxa spec='TaxonSet' alignment='@alignment1'/>
   </timeTraitSet>
\end{lstlisting}
   
Sampling times are not a requirement, and this piece can be removed altogether.



\subsection{Defining the substitution model}

The following block defines the substitution model PoMo:

\begin{lstlisting}
<siteModel spec="SiteModelPoMo" id="siteModel">
     <mutationRate spec='RealParameter' id="mutationRate" value="1.0"/>
     <substModel spec="PoMoGeneral" virtualPop="5" estimateRootFreqs="false" useTheta="false" id="PoMo.substModel">
       <fitness spec='RealParameter' id="PoMo.fitness" value="1.0 1.0 1.0 1.0"/>
       <rootFrequencies spec='RealParameter' id="PoMo.rootFreqs" value="0.25 0.25 0.25 0.25"/>
       <mutModel spec='MutationModel' id="PoMo.mutModel">
       		<rateVector spec='RealParameter' id="PoMo.mutRates" value="0.001 0.001"/>
       		<nucFreqs spec='RealParameter' id="PoMo.nucFreqs" value="0.25 0.25 0.25 0.25"/>
       </mutModel>
     </substModel>
   </siteModel>
\end{lstlisting}

In particular, this line defines many aspects of the model:
\begin{lstlisting}
     <substModel spec="PoMoGeneral" virtualPop="5" estimateRootFreqs="false" useTheta="false" id="PoMo.substModel">
\end{lstlisting}

For example, "virtualPop" determines how many individuals are in the virtual population (here 5, corresponding to PoMo5).
"estimateRootFreqs" determines if root nucleotide frequencies should be estimated or if equilibrium should be assumed.
Finally, "useTheta" specifies if a user-specified value of $\theta = 4N_e \mu$, should be used, or if it should be estimated.
if "useTheta" is set to "true", then $\theta$ can be set with:

\begin{lstlisting}
     <substModel spec="PoMoGeneral" virtualPop="5" estimateRootFreqs="false" useTheta="true" theta="0.016" id="PoMo.substModel">
\end{lstlisting}

The mutation model has to be one of the following: HKY85, GTR, or general non-reversible. The mutation model is specified in the following line:
\begin{lstlisting}
<rateVector spec='RealParameter' id="PoMo.mutRates" value="0.001 0.001"/>
\end{lstlisting}
Depending on how many values are given after "value", the mutation model is picked. the number of values mean the number of mutation rates in the model, excluding nucleotide frequencies: 1=F81, 2=HKY, 6=GTR, 12=general non-reversible




\subsection{Sequencing error}

Starting value for sequencing error is specified as:
\begin{lstlisting}
     <seqError spec='RealParameter' id="PoMo.seqError" value="0.0"/>
\end{lstlisting}

A starting value of "0.0" as shown above, means that no sequencing error is assumed.
If the value is different from 0, sequencing error is taken into account, and if an operator (see next section) for the sequencing error is specified, the error rate is estimated.



\subsection{Variation in population sizes and mutation rates}

Variation in the mutation rate in PoMo will not affect al the rates of the PoMo matrix, but only the mutation rates. This includes both variation along the genome and variation along branches.
However, evolution along the branches can also be affected by changes in effective population sizes (smaller population leads to faster drift).
To model variation in population sizes along the phylogeny, we allow the use of Random Local Clock (see https://bmcbiol.biomedcentral.com/articles/10.1186/1741-7007-8-114) except of course instead of the clock rate, it's the population size that changes from one branch to the next.
This model allows a different population size for each branch, but has a strong prior on only a few changes happening on the phylogeny, and on population sizes being usually inherited without changes.

A template of such model can be found in the example file "PoMo5\_branch\_popSizes.xml" and one important aspect that needs to be taken into account is the number of population sizes and indicator, which is $2n-2$ where $n$ is the number of tips in the tree. This value has to be inserted in the following rows:
\begin{lstlisting}
         <stateNode id="IndicatorsPoMo" spec="parameter.BooleanParameter" dimension="6">false</stateNode>
        <parameter id="popSizes" dimension="6" name="stateNode">1.0</parameter>
\end{lstlisting}

replacing "6" with the value of $2n-2$.



\subsection{Operators}

Operators determine which parameters of the model are modified, and which are held constant.
For example, the following code 
\begin{lstlisting}
     <!--operator spec="ScaleOperator" id="errScaler"
 	      parameter="@PoMo.seqError"
	      scaleFactor="0.9" weight="1"/-->
\end{lstlisting}

is commented out (same as if it is not there).
By including the same operator,
\begin{lstlisting}
     <operator spec="ScaleOperator" id="errScaler"
 	      parameter="@PoMo.seqError"
	      scaleFactor="0.9" weight="1"/>
\end{lstlisting}
 we tell to BEAST to alter the value, and therefore estimate, the considered parameter, in this case the sequencing error.
 
 Similarly, other operators can be included or removed/commented out, for example:
 
 \begin{lstlisting}
     <operator spec="ScaleOperator" id="fitScaler"
 	      parameter="@PoMo.fitness"
	      scaleFactor="0.8" weight="3"/>
\end{lstlisting}
for nucleotide fitnesses (selection or fixation bias of nucleotides);

 \begin{lstlisting}
     <operator spec="DeltaExchangeOperator" id="freqExchangerRoot"
	       parameter="@PoMo.rootFreqs"
	       delta="0.01" weight="0.5"/>
\end{lstlisting}
for root nucleotide frequencies (only to be used if we are estimating root nucleotide frequencies).

In the case of a nonreversible model, the following operator has to be removed:
 \begin{lstlisting}
     <operator spec="DeltaExchangeOperator" id="freqExchanger"
	       parameter="@PoMo.nucFreqs"
	       delta="0.01" weight="0.5"/>
\end{lstlisting}



\section{Running PoMo}

Just select the PoMo xml file you created, after you double-clicked on the BEAST2 icon in the BEAST2 folder.

Alternatively, you can get a copy of the PoMo package as a zip file from \emph{ https://bitbucket.org/nicofmay/pomo-beast/src/master/dist/}.
after unzipping it, and locating the jar file in the lib folder, you can run the following command from terminal:

\emph{java -cp /path\_to\_PoMo/PoMo.v0.1.0.jar:/path\_to\_BEAST/BEAST\ 2.3.1/lib/beast.jar beast.app.beastapp.BeastMain /path\_to\_xml\_file/file.xml}


\end{document}

