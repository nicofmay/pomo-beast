import sys
import os
import random
import math
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--inputF',"-i", help='cf file containing alignment).')
parser.add_argument('--outputF',"-o", help='name of output xml file.')
parser.add_argument('--Ne',"-N", help='number of individuals in the virtual population (default=15, larger values come at higher computational expense).', type=int, default=15)
parser.add_argument('--mcmc',"-m", help='number of mcmc iterations (default=500000, larger values come at higher computational cost).', type=int, default=500000)
parser.add_argument('--theta',"-t", help='do you want to estimate theta, the ancestral proportion of polymorphic sites? (default=False).', type=bool, default=False)
args = parser.parse_args()

virtualN=args.Ne
Nmcmc=args.mcmc
theta=args.theta

if(args.inputF=="" or args.outputF==""):
	print("Error, input and output files must be specified with -i and -o options.")
	exit()
    
## Read input file
inpF=open(args.inputF)
line=inpF.readline()
line=inpF.readline()
linelist=line.split()
nSpecies=len(linelist)-2
names=[]
seqs=[]
for n in range(len(linelist)-2):
    names.append(linelist[n+2])
    seqs.append([])
fixed={}
line=inpF.readline()
linelist=line.split()
numl=0
while line!="" and line!="\n":
    for n in range(len(linelist)-2):
        stri=linelist[n+2].replace(",","-")
        stri+=", "
        seqs[n].append(stri)
    line=inpF.readline()
    linelist=line.split()
inpF.close()
outF=open(args.outputF+".xml","w")
outF.write("<beast version='2.0'\n namespace=\'PoMo:beast.util:beast.core:beast.evolution:beast.math:beast.evolution.alignment:beast.core.parameter:beast.core.util:beast.evolution.likelihood:beast.evolution.sitemodel:beast.evolution.substitutionmodel:beast.math.distributions:beast.evolution.speciation:beast.evolution.operators\'>\n\n\n")
outF.write("<alignmentPoMo spec=\"alignment.pomo.evolution.AlignmentPoMo\" id=\"alignment\" dataTypePoMo=\"PoModata\">\n\n")
for n in range(len(names)):
    outF.write("	<sequence spec=\"SequencePoMo\" id=\""+names[n]+"\" taxon=\"taxon"+names[n]+"\" value=\""+"".join(seqs[n])+"\"/>\n\n")
outF.write("</alignmentPoMo>\n\n\n")

#outF.write("     <input spec=\'YuleModel\' id=\"yule\">\n        <parameter spec=\'birthDiffRate\' id=\"birthRate\" value=\"1\"/>\n            <tree spec=\'beast.evolution.tree.RandomTree\' id=\'tree\' estimate=\'true\'>\n                <taxa spec=\'Alignment\' idref=\'alignment\'/>\n                <populationModel id=\'ConstantPopulation0\' spec=\'beast.evolution.tree.coalescent.ConstantPopulation\'>\n            		<popSize id=\'randomPopSize\' spec=\'parameter.RealParameter\' value=\'1\'/>\n	            </populationModel>\n            </tree>\n     </input>\n\n\n")

outF.write("   <tree estimate=\"true\" id=\"tree\" name=\"stateNode\">\n       <taxonset id=\"TaxonSet.alignment\" spec=\"TaxonSet\">\n           <data idref=\"alignment\" name=\"alignment\"/>\n       </taxonset>\n   </tree>\n\n\n")
   
#outF.write("<input spec=\'YuleModel\' id=\"yule\">\n        <birthDiffRate idref=\"birthRate\"/>\n        <tree idref=\'tree\'/>\n    </input>\n    <parameter id=\"birthRate\" value=\"2.0\" lower=\"0.0\" upper=\"100.0\"/>\n\n\n")

outF.write("<!-- Substitution model (PoMo + HKY), theta (if used) is the frequency of polymorphisms at the root. If root nuc freqs are not estimated, the ones from mutation rates are used. -->\n    <siteModel spec=\"SiteModelPoMo\" id=\"siteModel\">\n     <mutationRate spec=\'RealParameter\' id=\"mutationRate\" value=\"0.05\" lower=\"0.0\"/>\n     <substModel spec=\"PoMoGeneral\" virtualPop=\""+str(virtualN)+"\" estimateRootFreqs=\"false\" useTheta=\""+str(theta)+"\" theta=\"0.01\" id=\"PoMo.substModel\">\n       <fitness spec=\'RealParameter\' id=\"PoMo.fitness\" value=\"1.0 1.0 1.0 1.0\" lower=\"0.8\" upper=\"1.2\"/>\n       <rootNucFreqs spec=\'RealParameter\' id=\"PoMo.rootFreqs\" value=\"0.25 0.25 0.25 0.25\"/>\n       <mutModel spec=\'MutationModel\' id=\"PoMo.mutModel\">\n       		<rateVector spec=\'RealParameter\' id=\"PoMo.mutRates\" value=\"0.01 0.01\" upper=\"0.3\"/>\n       		<nucFreqs spec=\'RealParameter\' id=\"PoMo.nucFreqs\" value=\"0.25 0.25 0.25 0.25\"/>\n       </mutModel>\n     </substModel>\n   </siteModel>\n\n\n")

outF.write("  <input spec=\'CompoundDistribution\' id=\'parameterPriors\'>\n     <distribution spec=\'beast.math.distributions.Prior\' x=\"@PoMo.mutRates\">\n       <distr spec=\'LogNormalDistributionModel\' M=\"-4.0\" S=\"8.0\"/>\n     </distribution>\n   </input>\n\n\n")#     <distribution spec=\'beast.math.distributions.Prior\' x=\"@mutationRate\">\n       <distr spec=\'LogNormalDistributionModel\' M=\"0.0\" S=\"4.0\"/>\n     </distribution>\n\n     <distribution id=\"yule.prior\" idref=\'yule\'/>\n

outF.write("   <input spec=\'TreeLikelihoodPoMo\' id=\"treeLikelihood1\">\n     <seqError spec=\'RealParameter\' id=\"PoMo.seqError\" value=\"0.0\"/>\n     <data idref=\"alignment\"/>\n     <tree idref=\"tree\"/>\n     <siteModel idref=\'siteModel\'/>\n   </input>\n\n\n")

outF.write("   <run spec=\"MCMC\" id=\"mcmc\" chainLength=\""+str(Nmcmc)+"\" storeEvery=\"100000\">\n     <state>\n       <stateNode idref=\"tree\"/>\n       <stateNode idref=\"mutationRate\"/>\n       <stateNode idref=\"PoMo.fitness\"/>\n       <stateNode idref=\"PoMo.mutRates\"/>\n       <stateNode idref=\"PoMo.seqError\"/>\n       <stateNode idref=\"PoMo.nucFreqs\"/>\n       <stateNode idref=\"PoMo.rootFreqs\"/>\n     </state>\n\n     <distribution spec=\'CompoundDistribution\' id=\'posterior\'>\n       <distribution idref=\"treeLikelihood1\"/>\n       <distribution idref=\"parameterPriors\"/>\n     </distribution>\n\n\n")#       <stateNode idref=\'birthRate\'/>\n

outF.write("    <operator spec=\'ScaleOperator\' id=\'mutScaler\'\n 	      parameter=\"@PoMo.mutRates\"\n 	      scaleFactor=\"0.98\" weight=\"5\"/>\n\n	 <operator spec=\"DeltaExchangeOperator\" id=\"freqExchanger\"\n	       parameter=\"@PoMo.nucFreqs\"\n	       delta=\"0.0002\" weight=\"5\"/>\n\n		<operator id=\'treeScaler\' spec=\'ScaleOperator\' scaleFactor=\"0.75\" weight=\"3\" tree=\"@tree\"/>\n        <operator spec=\'beast.evolution.operators.Uniform\' weight=\"30\" tree=\"@tree\"/>\n        <operator spec=\'SubtreeSlide\' weight=\"15\" gaussian=\"true\" size=\"0.0077\" tree=\"@tree\"/>\n        <operator id=\'narrow\' spec=\'Exchange\' isNarrow=\'true\' weight=\"15\" tree=\"@tree\"/>\n        <operator id=\'wide\' spec=\'Exchange\' isNarrow=\'false\' weight=\"3\" tree=\"@tree\"/>\n        <operator spec=\'WilsonBalding\' weight=\"1\" tree=\"@tree\"/>\n        <operator spec=\'UpDownOperator\' scaleFactor=\"0.75\" weight=\"3\" down=\"@tree\"/>\n\n\n")#     <operator spec=\'ScaleOperator\' id=\'birthScaler\'\n	       parameter=\"@birthRate\"\n	       scaleFactor=\"0.8\" weight=\"0.3\"/>\n     <operator spec=\"ScaleOperator\" id=\"muRateScaler\"\n	       parameter=\"@mutationRate\"\n	       scaleFactor=\"0.8\" weight=\"0.3\"/>\n\n
    
outF.write("    <logger logEvery=\"100\" fileName=\""+args.outputF+".log\">\n       <model idref=\'posterior\'/>\n       <log idref=\"posterior\"/>\n       <log idref=\"parameterPriors\"/>\n       <log idref=\"treeLikelihood1\"/>\n       <log spec=\'beast.evolution.tree.TreeHeightLogger\' tree=\'@tree\'/>\n       <log idref=\"PoMo.mutRates\"/>\n       <log idref=\"PoMo.nucFreqs\"/>\n     </logger>\n\n       <logger logEvery=\"100\" fileName=\""+args.outputF+".trees\">\n            <log idref=\"tree\"/>\n        </logger>\n\n     <logger logEvery=\"10000\">\n       <model idref=\'posterior\'/>\n       <log idref=\"posterior\"/>\n       <log idref=\"parameterPriors\"/>\n       <log idref=\"treeLikelihood1\"/>\n       <log spec=\'beast.evolution.tree.TreeHeightLogger\' tree=\'@tree\'/>\n       <log idref=\"PoMo.mutRates\"/>\n       <log idref=\"PoMo.nucFreqs\"/>\n       <ESS spec=\'ESS\' name=\'log\' arg=\"@posterior\"/>\n       <ESS spec=\'ESS\' name=\'log\' arg=\"@treeLikelihood1\"/>\n     </logger>\n\n   </run>\n </beast>\n")#       <log idref=\"yule.prior\"/>\n        <log idref=\"birthRate\"/>\n       <log idref=\"mutationRate\"/>\n

outF.close()

exit()