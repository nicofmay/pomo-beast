/*
* File TreeLikelihood.java
*
* Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
*
* This file is part of BEAST2.
* See the NOTICE file distributed with this work for additional
* information regarding copyright ownership and licensing.
*
* BEAST is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
*  BEAST is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with BEAST; if not, write to the
* Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
* Boston, MA  02110-1301  USA
*/


package pomo.evolution.likelihood;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

//import org.jblas.DoubleMatrix;
//import org.jblas.MatrixFunctions;

import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.evolution.likelihood.GenericTreeLikelihood;
import beast.base.evolution.likelihood.LikelihoodCore;
import beast.base.inference.State;
import beast.base.core.Input.Validate;
import beast.base.inference.parameter.RealParameter;
//import beast.base.core.Input.Validate;
//import beast.base.inference.parameter.RealParameter;
import beast.base.core.Log;
//import beast.base.evolution.alignment.Alignment;
import pomo.evolution.alignment.AlignmentPoMo;
import beast.base.evolution.branchratemodel.BranchRateModel;
import pomo.evolution.branchratemodel.BranchRateModel_PoMo;
import beast.base.evolution.branchratemodel.StrictClockModel;
import pomo.evolution.branchratemodel.StrictPopSizeModel;
import pomo.evolution.datatype.DataTypePoMo;
//import beast.base.evolution.datatype.DataType;
//import beast.base.evolution.sitemodel.SiteModel;
//import beast.base.evolution.sitemodel.SiteModelInterface;
//import beast.base.evolution.sitemodel.SiteModelInterfacePoMo;
import pomo.evolution.sitemodel.SiteModelInterfacePoMo.Base;
import pomo.evolution.sitemodel.SiteModelPoMo;
import pomo.evolution.substitutionmodel.PoMoGeneral;
//import beast.evolution.tree.MultiTypeNodeVolz;
//import beast.base.evolution.substitutionmodel.SubstitutionModel;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeInterface;
import beast.base.util.Randomizer;
//import beast.matrix.MatrixExponentiator;
//import beast.matrix.PoMoMatrixSparse;
//import beast.matrix.AbstractMatrix;
//import beast.matrix.Array2d;





@Description("Calculates the probability of sequence data on a beast.tree given a site and PoMo substitution model using " +
        "a variant of the 'peeling algorithm'. For details, see" +
        "Felsenstein, Joseph (1981). Evolutionary trees from DNA sequences: a maximum likelihood approach. J Mol Evol 17 (6): 368-376.")
public class TreeLikelihoodPoMo extends GenericTreeLikelihood {

    public Input<Boolean> m_useAmbiguities = new Input<Boolean>("useAmbiguities", "flag to indicate leafs that sites containing ambigue states should be handled instead of ignored (the default)", false);
    
    public Input<Boolean> tryBeagle = new Input<Boolean>("tryBeagle", "flag to indicate if trying to use Beagle (the default). For ancestral state reconstruction, set to false", true);
    
    public Input<RealParameter> seqErrorInput = new Input<RealParameter>("seqError", " starting sequencing error rate, 0 by default", Validate.REQUIRED);
    
    enum Scaling {none, always, _default};
    public Input<Scaling> scaling = new Input<TreeLikelihoodPoMo.Scaling>("scaling", "type of scaling to use, one of " + Arrays.toString(Scaling.values()) + ". If not specified, the -beagle_scaling flag is used.", Scaling._default, Scaling.values());
    
    final public Input<BranchRateModel_PoMo.Base> branchRateModelPoMoInput = new Input<>("branchRateModelPoMo",
            "A model describing pop sizes on the branches of the beast.tree.");
    
    //public Input<AlignmentPoMo2> dataInput = new Input<AlignmentPoMo2>("data", "sequence data for the beast.tree", Validate.REQUIRED);
    
    //public Input<SiteModelInterfacePoMo> siteModelInput = new Input<SiteModelInterfacePoMo>("siteModel", "site model for leafs in the beast.tree", Validate.REQUIRED);

    /**
     * calculation engine *
     */
    protected LikelihoodCore likelihoodCore;
    BeagleTreeLikelihoodPoMo beagle;
    
    RealParameter seqErr;
    double sErr, sErrStored;
    int nS;
    //public boolean loggable=true;

    /**
     * Plugin associated with inputs. Since none of the inputs are StateNodes, it
     * is safe to link to them only once, during initAndValidate.
     */
    PoMoGeneral substitutionModel;
    protected SiteModelPoMo.Base m_siteModel;
    protected BranchRateModel.Base branchRateModel;
    protected BranchRateModel_PoMo.Base branchRateModelPoMo;

    /**
     * flag to indicate the
     * // when CLEAN=0, nothing needs to be recalculated for the node
     * // when DIRTY=1 indicates a node partial needs to be recalculated
     * // when FILTHY=2 indicates the indices for the node need to be recalculated
     * // (often not necessary while node partial recalculation is required)
     */
    protected int hasDirt;

    /**
     * Lengths of the branches in the tree associated with each of the nodes
     * in the tree through their node  numbers. By comparing whether the
     * current branch length differs from stored branch lengths, it is tested
     * whether a node is dirty and needs to be recomputed (there may be other
     * reasons as well...).
     * These lengths take branch rate models in account.
     */
    protected double[] m_branchLengths;
    protected double[] storedBranchLengths;

    /**
     * memory allocation for likelihoods for each of the patterns *
     */
    double[] patternLogLikelihoods;
    /**
     * memory allocation for the root partials *
     */
    double[] m_fRootPartials;
    /**
     * memory allocation for probability tables obtained from the SiteModel *
     */
    double[] probabilities;

    int matrixSize;

    /**
     * flag to indicate ascertainment correction should be applied *
     */
    boolean useAscertainedSitePatterns = false;

    /**
     * dealing with proportion of site being invariant *
     */
    double proportionInvariant = 0;
    List<Integer> constantPattern = null;
    

    @Override
    public void initAndValidate() throws IllegalArgumentException {
    	
    	seqErr = seqErrorInput.get();
		sErr = seqErr.getValue();
		sErrStored = sErr;
		partialsDirty = true;
    	
        // sanity check: alignment should have same #taxa as tree
        if (dataInput.get().getTaxonCount() != treeInput.get().getLeafNodeCount()) {
            throw new IllegalArgumentException("The number of nodes in the tree does not match the number of sequences");
        }
        beagle = null;
        if (tryBeagle.get()){
	        beagle = new BeagleTreeLikelihoodPoMo();
	        try {
		        beagle.initByName(
	                    "data", dataInput.get(), "tree", treeInput.get(), "siteModel", ((SiteModelPoMo) siteModelInput.get()),
	                    "branchRateModel", branchRateModelInput.get(), "useAmbiguities", m_useAmbiguities.get(),
	                    "scaling", scaling.get().toString());
		        if (beagle.beagle != null) {
		            //a Beagle instance was found, so we use it
		        	System.out.println("Used beagle\n");
		            return;
		        }
	        } catch (Exception e) {
	        	System.out.println("Not using Beagle\n");
				// ignore
			}
	        // No Beagle instance was found, so we use the good old java likelihood core
	        beagle = null;
        }

        int nodeCount = treeInput.get().getNodeCount();
        if (!((siteModelInput.get()) instanceof SiteModelPoMo.Base)) {
        	throw new IllegalArgumentException("siteModel input should be of type SiteModel.Base");
        }
        m_siteModel = (Base) siteModelInput.get();
        m_siteModel.setDataType((DataTypePoMo)(dataInput.get().getDataType()));
        substitutionModel = m_siteModel.substModelInput.get();

        if (branchRateModelInput.get() != null) {
            branchRateModel = branchRateModelInput.get();
        } else {
            branchRateModel = new StrictClockModel();
        }
        if (branchRateModelPoMoInput.get() != null) {
            branchRateModelPoMo = branchRateModelPoMoInput.get();
        } else {
            branchRateModelPoMo = new StrictPopSizeModel();
        }
        m_branchLengths = new double[nodeCount];
        storedBranchLengths = new double[nodeCount];

        int nStateCount = dataInput.get().getMaxStateCount();
        nStateCount=substitutionModel.getStateCount();
        nS=nStateCount;
        int nPatterns = dataInput.get().getPatternCount();
        likelihoodCore = new BeerLikelihoodCoreBasic(nStateCount);

        String className = getClass().getSimpleName();

        AlignmentPoMo alignment = (AlignmentPoMo) dataInput.get();

        Log.info.println(className + "(" + getID() + ") uses " + likelihoodCore.getClass().getSimpleName());
        Log.info.println("  " + alignment.toString(true));
        // print startup messages via Log.print*

        proportionInvariant = m_siteModel.getProportionInvariant();
        m_siteModel.setPropInvariantIsCategory(false);
        if (proportionInvariant > 0) {
            calcConstantPatternIndices(nPatterns, nStateCount);
        }

        initCore();

        patternLogLikelihoods = new double[nPatterns];
        m_fRootPartials = new double[nPatterns * nStateCount];
        matrixSize = (nStateCount + 1) * (nStateCount + 1);
        probabilities = new double[(nStateCount + 1) * (nStateCount + 1)];
        Arrays.fill(probabilities, 1.0);

        if (dataInput.get().isAscertained) {
            useAscertainedSitePatterns = true;
        }
        
    }


    /**
     * Determine indices of m_fRootProbabilities that need to be updates
     * // due to sites being invariant. If none of the sites are invariant,
     * // the 'site invariant' category does not contribute anything to the
     * // root probability. If the site IS invariant for a certain character,
     * // taking ambiguities in account, there is a contribution of 1 from
     * // the 'site invariant' category.
     */
    void calcConstantPatternIndices(final int nPatterns, final int nStateCount) {
        constantPattern = new ArrayList<Integer>();
        for (int i = 0; i < nPatterns; i++) {
           // final int[][] pattern = dataInput.get().getPatternPoMo(i);
            final boolean[] bIsInvariant = new boolean[nStateCount];
            Arrays.fill(bIsInvariant, true);
            for (int k = 0; k < nStateCount; k++) {
                if (bIsInvariant[k]) {
                    constantPattern.add(i * nStateCount + k);
                }
            }
        }
    }

    void initCore() {
        final int nodeCount = treeInput.get().getNodeCount();
        likelihoodCore.initialize(
                nodeCount,
                dataInput.get().getPatternCount(),
                m_siteModel.getCategoryCount(),
                true, m_useAmbiguities.get()
        );

        final int extNodeCount = nodeCount / 2 + 1;
        final int intNodeCount = nodeCount / 2;

        sErr = seqErr.getValue();
        setPartials(treeInput.get().getRoot(), dataInput.get().getPatternCount());
        setCurrentPartials(treeInput.get().getRoot(), dataInput.get().getPatternCount());
        
        hasDirt = Tree.IS_FILTHY;
        for (int i = 0; i < intNodeCount; i++) {
            likelihoodCore.createNodePartials(extNodeCount + i);
        }
    }

    /**
     * This method samples the sequences based on the tree and site model.
     */
    public void sample(State state, Random random) {
        throw new UnsupportedOperationException("Can't sample a fixed alignment!");
    }

    /**
     * set leaf states in likelihood core *
     */
    void setStates(Node node, int patternCount) {
    	System.out.println("Warning, setting states in PoMo, no reason to.");
    }

    protected int getTaxonNr(Node node, AlignmentPoMo data) {
        int iTaxon = data.getTaxonIndex(node.getID());
        if (iTaxon == -1) {
        	if (node.getID().startsWith("'") || node.getID().startsWith("\"")) {
        		iTaxon = data.getTaxonIndex(node.getID().substring(1,node.getID().length()-1));
        	}
            if (iTaxon == -1) {
            	throw new RuntimeException("Could not find sequence " + node.getID() + " in the alignment");
            }
        }
        return iTaxon;
	}

	/**
     * set leaf partials in likelihood core *
     */
    //most important modified bit - Nicola
    void setPartials(Node node, int patternCount) {
        if (node.isLeaf()) {
        	double[] partials=getPartials(node, patternCount);
            likelihoodCore.setNodePartials(node.getNr(), partials);

        } else {
            setPartials(node.getLeft(), patternCount);
            setPartials(node.getRight(), patternCount);
        }
    }
    
    
    /**
     * set leaf partials in likelihood core *
     */
    //most important modified bit - Nicola
    void setCurrentPartials(Node node, int patternCount) {
    	((BeerLikelihoodCoreBasic)likelihoodCore).setNodePartialsForUpdate(node.getNr());
        if (node.isLeaf()) {
        	double[] partials=getPartials(node, patternCount);
            ((BeerLikelihoodCoreBasic)likelihoodCore).setCurrentNodePartials(node.getNr(), partials);

        } else {
            setCurrentPartials(node.getLeft(), patternCount);
            setCurrentPartials(node.getRight(), patternCount);
        }
    }

    
    
    double[] getPartials(Node node, int patternCount){
    	
        AlignmentPoMo data = (AlignmentPoMo) dataInput.get();
        int nStates = substitutionModel.getStateCount();
        int vPop=((nStates-4)/6)+1;
        double[] partials = new double[patternCount * nStates];

        int k = 0;
        int iTaxon = getTaxonNr(node, data);
        int[] nState, maxNucs, maxVals;
        for (int iPattern = 0; iPattern < patternCount; iPattern++) {
            nState = data.getPatternPoMo(iTaxon, iPattern);
            maxNucs = new int[]{-1, -1};
            maxVals = new int[]{0, 0};
            for (int iNuc = 0; iNuc < 4; iNuc++) {
            	if (nState[iNuc]>maxVals[0]){
            		maxNucs[1]=maxNucs[0];
            		maxVals[1]=maxVals[0];
            		maxNucs[0]=iNuc;
            		maxVals[0]=nState[iNuc];
            	}else if(nState[iNuc]>maxVals[1]){
            		maxNucs[1]=iNuc;
            		maxVals[1]=nState[iNuc];
            	}
            }
            if(maxVals[1]>0){
            	int numSeconds=0;
            	boolean equalFirst=false;
            	if (maxVals[1]==maxVals[0])equalFirst=true;
            	for (int iNuc = 0; iNuc < 4; iNuc++) {
            		if(nState[iNuc]==maxVals[1]) numSeconds+=1;
            	}
            	if(numSeconds>1){ //if there are multiple nucleotides sharing the second place of most abundant, than pick one at random as second most abundant.
            		int newSecond=Randomizer.nextInt(numSeconds);
            		int secondCount=0;
            		for (int iNuc = 0; iNuc < 4; iNuc++) {
                		if(nState[iNuc]==maxVals[1] && secondCount==newSecond) maxNucs[1]=iNuc;
                		else secondCount+=1;
                	}
            		if(equalFirst){
            			int newFirst=-1;
            			if(numSeconds==2){ newFirst=1-newSecond;
            			}else{
            				newFirst=Randomizer.nextInt(numSeconds-1);
            				if (newFirst>=newSecond) newFirst+=1;
            				
            			}
            			int firstCount=0;
                		for (int iNuc = 0; iNuc < 4; iNuc++) {
                    		if(nState[iNuc]==maxVals[1] && firstCount==newFirst) maxNucs[0]=iNuc;
                    		else firstCount+=1;
                    	}
            		}
            	}
            }
            if(maxNucs[0]==-1){
            	for (int iState = 0; iState < nStates; iState++) {
                    partials[k++] = 1.0;
                }
            }else {
            	//fixed states
            	for (int iState = 0; iState < 4; iState++) {
            		if (iState==maxNucs[0]){
            			partials[k++] = sampProb(1.0-sErr,maxVals[0],sErr/3.0,maxVals[1]);
            		} else if (iState==maxNucs[1]){
            			partials[k++] = sampProb(sErr/3.0,maxVals[0],1.0-sErr,maxVals[1]);
            		} else{
            			partials[k++] = sampProb(sErr/3.0,maxVals[0]+maxVals[1],1.0-sErr,0);
            		}
            	}
            	//groups of polymorphisms
            	for (int iGroup = 0; iGroup < 6; iGroup++) {
            		int[] ind=PoMoGeneral.indeces(iGroup);
            		for (int iState = 0; iState < vPop-1; iState++) {
                		double[] probs=new double[2];
                		double freq;
                		if (maxNucs[0]==ind[0]) {
                			freq=(iState+1.0)/((double)(vPop));
                			//probs[0]=freq*(1.0-sErr) + (1.0-freq)*sErr/3.0;
                		}else if(maxNucs[0]==ind[1]) {
                			freq=(vPop - (iState+1.0))/((double)(vPop));
                			//probs[0]=(vPop - (iState+1.0))/((double)(vPop));
                		}else{
                			freq=0.0;
                			//probs[0]=sErr;
                		}
                		probs[0]=freq*(1.0-sErr) + (1.0-freq)*sErr/3.0;
                		if (maxNucs[1]==ind[0]) {
                			freq=(iState+1.0)/((double)(vPop));
                			//probs[1]=(iState+1.0)/((double)(vPop));
                		}else if(maxNucs[1]==ind[1]) {
                			freq=(vPop - (iState+1.0))/((double)(vPop));
                			//probs[1]=(vPop - (iState+1.0))/((double)(vPop));
                		}else{
                			freq=0.0;
                			//probs[1]=sErr;
                		}
                		probs[1]=freq*(1.0-sErr) + (1.0-freq)*sErr/3.0;
                		partials[k++] = sampProb(probs[0],maxVals[0],probs[1],maxVals[1]);
            		}
            	}
            }
//            if (iPattern==0){
//            	String s="Pattern: "+maxNucs[0]+"-"+maxVals[0]+", "+maxNucs[1]+"-"+maxVals[1]+" \n    New Partials: ";
//                for (int i = 0; i < nStates; i++) {
//                	s+=", "+partials[i];
//                }
//                System.out.println(s);
//            }
        }
    	return partials;
    }
    
    
    //the partial likelihood of sampling - Nicola
    double sampProb(double p1, int v1, double p2, int v2){
    	double res=1.0,bigP,smallP;
    	int big,small;
    	if (v1>v2) {
    		big=v1;
    		bigP=p1;
    		small=v2;
    		smallP=p2;
    	}else{
    		big=v2;
    		bigP=p2;
    		small=v1;
    		smallP=p1;
    	}
    			
    	for (int i = 1; i <= small; i++) {
    		res=res*(i+big)*smallP*bigP/i;
    	}
    	for (int i = 1; i <= big-small; i++) {
    		res=res*bigP;
    	}
    	return res;
    }
    
    
    

    /**
     * Calculate the log likelihood of the current state.
     *
     * @return the log likelihood.
     */
    double m_fScale = 1.01;
    int m_nScale = 0;
    int X = 100;
    boolean partialsDirty;

    @Override
    public double calculateLogP() throws IllegalArgumentException {
    	
    	if(partialsDirty){
    		sErr = seqErr.getValue();
    	}
    	
        if (beagle != null) {
            try {
				logP = beagle.calculateLogPPartials(partialsDirty, sErr);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            partialsDirty = false;
            //System.out.println("Calculating likelihood, sErrStored="+sErrStored+"   SErr="+sErr+"  logP="+logP);
            return logP;
        }
        
    	if(partialsDirty){
    		//System.out.println("Setting new partials "+sErr);
    		//((BeerLikelihoodCore)likelihoodCore).unstore();
    		setCurrentPartials(treeInput.get().getRoot(), dataInput.get().getPatternCount());
    		hasDirt = Tree.IS_FILTHY;
    		partialsDirty=false;
    	}
    	
        final TreeInterface tree = treeInput.get();

        try {
			if ((traverse(tree.getRoot()) != Tree.IS_CLEAN))	calcLogP();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        //partialsDirty = false;
        //System.out.println("Calculating likelihood, sErrStored="+sErrStored+"   SErr="+sErr+"  logP="+logP);

        m_nScale++;
        if (logP > 0 || (likelihoodCore.getUseScaling() && m_nScale > X)) {
//            System.err.println("Switch off scaling");
//            m_likelihoodCore.setUseScaling(1.0);
//            m_likelihoodCore.unstore();
//            m_nHasDirt = Tree.IS_FILTHY;
//            X *= 2;
//            traverse(tree.getRoot());
//            calcLogP();
//            return logP;
        } else if (logP == Double.NEGATIVE_INFINITY && m_fScale < 10 && !scaling.get().equals(Scaling.none)) { // && !m_likelihoodCore.getUseScaling()) {
            m_nScale = 0;
            m_fScale *= 1.01;
            System.err.println("Turning on scaling to prevent numeric instability " + m_fScale);
            likelihoodCore.setUseScaling(m_fScale);
            likelihoodCore.unstore();
            hasDirt = Tree.IS_FILTHY;
            try {
				traverse(tree.getRoot());
				calcLogP();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            return logP;
        }
        return logP;
    }

    void calcLogP() throws Exception {
        logP = 0.0;
        if (useAscertainedSitePatterns) {
            final double ascertainmentCorrection = dataInput.get().getAscertainmentCorrection(patternLogLikelihoods);
            for (int i = 0; i < dataInput.get().getPatternCount(); i++) {
                logP += (patternLogLikelihoods[i] - ascertainmentCorrection) * dataInput.get().getPatternWeight(i);
            }
        } else {
            for (int i = 0; i < dataInput.get().getPatternCount(); i++) {
                logP += patternLogLikelihoods[i] * dataInput.get().getPatternWeight(i);
            }
        }
        if (logP>0.0){
        	System.out.println("log-likelihood>0, probably matrix exponentiation problem, rejecting move\n");
        	logP=Double.NEGATIVE_INFINITY;
        }
    }

    /* Assumes there IS a branch rate model as opposed to traverse() */
    int traverse(final Node node) throws Exception {

        int update = (node.isDirty() | hasDirt);

        final int iNode = node.getNr();

        final double branchRate = branchRateModel.getRateForBranch(node);
        final double branchPopSize = branchRateModelPoMo.getPopSizeForBranch(node);
        final double branchTime = node.getLength() * branchRate;

        // First update the transition probability matrix(ices) for this branch
        if (!node.isRoot() && (update != Tree.IS_CLEAN || branchTime != m_branchLengths[iNode])) {
            m_branchLengths[iNode] = branchTime;
            final Node parent = node.getParent();
            likelihoodCore.setNodeMatrixForUpdate(iNode);
            for (int i = 0; i < m_siteModel.getCategoryCount(); i++) {
                final double jointBranchRate = m_siteModel.getRateForCategory(i, node) * branchRate;
                substitutionModel.getTransitionProbabilities(node, parent.getHeight(), node.getHeight(), jointBranchRate, probabilities, branchPopSize);
                //System.out.println(node.getNr() + " " + Arrays.toString(m_fProbabilities));
                likelihoodCore.setNodeMatrix(iNode, i, probabilities);
            }
            update |= Tree.IS_DIRTY;
        }

        // If the node is internal, update the partial likelihoods.
        if (!node.isLeaf()) {

            // Traverse down the two child nodes
            final Node child1 = node.getLeft(); //Two children
            final int update1 = traverse(child1);

            final Node child2 = node.getRight();
            final int update2 = traverse(child2);

            // If either child node was updated then update this node too
            if (update1 != Tree.IS_CLEAN || update2 != Tree.IS_CLEAN) {

                final int childNum1 = child1.getNr();
                final int childNum2 = child2.getNr();

                likelihoodCore.setNodePartialsForUpdate(iNode);
                update |= (update1 | update2);
                if (update >= Tree.IS_FILTHY) {
                    likelihoodCore.setNodeStatesForUpdate(iNode);
                }

                if (m_siteModel.integrateAcrossCategories()) {
                    likelihoodCore.calculatePartials(childNum1, childNum2, iNode);
                } else {
                    throw new Exception("Error TreeLikelihood 201: Site categories not supported");
                    //m_pLikelihoodCore->calculatePartials(childNum1, childNum2, nodeNum, siteCategories);
                }

                if (node.isRoot()) {
                    // No parent this is the root of the beast.tree -
                    // calculate the pattern likelihoods
                    final double[] frequencies = //m_pFreqs.get().
                            substitutionModel.getFrequencies();

                    final double[] proportions = m_siteModel.getCategoryProportions(node);
                    likelihoodCore.integratePartials(node.getNr(), proportions, m_fRootPartials);

                    if (constantPattern != null) { // && !SiteModel.g_bUseOriginal) {
                    	System.out.println("Constant patterns? ");
                        proportionInvariant = m_siteModel.getProportionInvariant();
                        // some portion of sites is invariant, so adjust root partials for this
                        for (final int i : constantPattern) {
                            m_fRootPartials[i] += proportionInvariant;
                        }
                    }

                    likelihoodCore.calculateLogLikelihoods(m_fRootPartials, frequencies, patternLogLikelihoods);
                }

            }
        }
        return update;
    } // traverseWithBRM

    /** CalculationNode methods **/

    /**
     * check state for changed variables and update temp results if necessary *
     */
    @Override
    protected boolean requiresRecalculation() {
    	if(seqErr.somethingIsDirty()){
        	partialsDirty = true;
        	//System.out.println("PartialsDirty is true");
        }
    	
        if (beagle != null) {
            return (beagle.requiresRecalculationFromLike(partialsDirty,sErr)||partialsDirty);
        }
        hasDirt = Tree.IS_CLEAN;

        if (dataInput.get().isDirtyCalculation()) {
            hasDirt = Tree.IS_FILTHY;
            return true;
        }
        if (m_siteModel.isDirtyCalculation()) {
            hasDirt = Tree.IS_DIRTY;
            return true;
        }
        if (branchRateModel != null && branchRateModel.isDirtyCalculation()) {
            //m_nHasDirt = Tree.IS_DIRTY;
            return true;
        }
        if (branchRateModelPoMo != null && branchRateModelPoMo.isDirtyCalculation()) {
            //m_nHasDirt = Tree.IS_DIRTY;
            return true;
        }
        
        if(partialsDirty){
        	return true;
        }
        
        return treeInput.get().somethingIsDirty();
    }

    @Override
    public void store() {
    	sErrStored=sErr;
        if (beagle != null) {
            beagle.store();
            super.store();
            return;
        }
        if (likelihoodCore != null) {
            likelihoodCore.store();
        }
        super.store();
        System.arraycopy(m_branchLengths, 0, storedBranchLengths, 0, m_branchLengths.length);
    }

    @Override
    public void restore() {
    	sErr=sErrStored;
    	partialsDirty=false;
        if (beagle != null) {
            beagle.restore();
            super.restore();
            return;
        }
        if (likelihoodCore != null) {
            likelihoodCore.restore();
        }
        super.restore();
        double[] tmp = m_branchLengths;
        m_branchLengths = storedBranchLengths;
        storedBranchLengths = tmp;
    }

    /**
     * @return a list of unique ids for the state nodes that form the argument
     */
    public List<String> getArguments() {
        return Collections.singletonList(dataInput.get().getID());
    }

    /**
     * @return a list of unique ids for the state nodes that make up the conditions
     */
    public List<String> getConditions() {
        return m_siteModel.getConditions();
    }
    
    
    
    
    

    /**
     * set one deme at the root picked at random from the probability distribution of demes at the root.
     */
    public void setRootDeme() {
    	 //System.out.println("setRootDeme start.");
    	 //int nStates = substitutionModel.getStateCount();
    	 int nStates = nS;
    	 int patternCount = dataInput.get().getPatternCount();
    	 double[] partials = new double[patternCount*nStates]; 
    	 Node root=treeInput.get().getRoot();
    	 likelihoodCore.getNodePartials(root.getNr(), partials);
    	 final double[] frequencies = substitutionModel.getFrequencies();
    	 int[] types= new int[patternCount];
    	 for (int p = 0; p<patternCount; p++) {
    		 types[p]=-1;
    		 double[] probs = new double[nStates];
    		 for (int i = 0; i<nStates; i++) {
    			 probs[i]=partials[p*nStates + i]*frequencies[i];
    		 }
    		 double sum=0.0;
    	     for (int i = 0; i<probs.length; i++) {
    	    	 //System.out.println("Probability at index "+i+" = "+probs[i]);
    	    	 sum+=probs[i];
    	     }
    	     for (int i = 0; i<probs.length; i++) {
    	    	 //System.out.println("Probability at index "+i+" = "+probs[i]);
    	    	 probs[i]=probs[i]/sum;
    	     }
    		 //int type=-1;
    	     //System.out.println("\n New root\n");
    	     double x = Randomizer.nextDouble();
    	     sum=0.0;
    	     for (int i = 0; i<probs.length; i++) {
    	    	 //System.out.println("Probability at index "+i+" = "+probs[i]);
    	    	 sum+=probs[i];
    	    	 if (x<sum){
    	    		types[p]=i;
    	    		break;
    	    	 }
    	     }

	    	if (types[0]==-1) {
	    		System.out.println("Something is wrong with probabilities at the root.");
	    		for (int i = 0; i<probs.length; i++) {
	    			System.out.println("Probability at index "+i+" = "+probs[i]+"\n");
	        	}
	    		System.exit(0);
	    	}
    	 }
    	 //likelihoodCore.getNodeStates(nodeIndex, states);
    	 likelihoodCore.setNodeStates(root.getNr(), types);
    	 //System.out.println("setRootDeme end.");
    }
    
    
    /**
     * set all demes at internal nodes, picking at random from their probability conditioned on their subtree leaves, 
     * and conditioned on the deme sampled for their parent.
     */
    public void setAllDemes() {
    	//System.out.println("setAllDemes start.");
    	setRootDeme();
    	Node root=treeInput.get().getRoot();
    	//System.out.println("Root has deme "+root.getNodeType()+"\n");
    	//Then make iteration
    	int patternCount = dataInput.get().getPatternCount();
    	int[] states= new int[patternCount];
    	likelihoodCore.getNodeStates(root.getNr(), states);
    	if ((root.getLeft()!=null)){// && (!root.getLeft().isLeaf())
    		Node LC= root.getLeft();
        	double LCTime= root.getHeight() - LC.getHeight();
        	setDemeIterative(root.getLeft(),LCTime,states);//root.getNodeType()
    	}
    	
    	if ((root.getRight()!=null)){ //&& (!root.getRight().isLeaf())
    		Node RC= root.getRight();
        	double RCTime= root.getHeight() - RC.getHeight();
        	setDemeIterative(root.getRight(),RCTime,states);
    	}
    	//System.out.println("setAllDemes end.");
    }
    

    
    /**
     * Set the deme for a node, and then call the function iteratively for the leaves.
     * and conditioned on the deme sampled for their parent.
     */
    public void setDemeIterative(Node node, double time, int[] parentStates) {
    	//System.out.println("setDemeIterative start.");
    	//int nTypes = substitutionModel.getStateCount();
    	int nTypes = nS;
   	 	int patternCount = dataInput.get().getPatternCount();
   	 	int nStates = nS;
   	 	double[] categoryFreqs = new double[m_siteModel.getCategoryCount()];
   	 	categoryFreqs = m_siteModel.getCategoryProportions(node);
   	 	double[] partials = new double[patternCount*nStates]; 
   	 	likelihoodCore.getNodePartials(node.getNr(), partials);
   	 	int[] types= new int[patternCount];
   	 	double[] transProbs = new double[(nStates) * (nStates)];
   	 	for (int p = 0; p < patternCount; p++) {
   	 		double[] probs = new double[nStates];
		   	for (int i = 0; i < m_siteModel.getCategoryCount(); i++) {
		   		likelihoodCore.getNodeMatrix(node.getNr(), i, transProbs);
		   		for (int s = 0; s < nStates; s++) {
		   			probs[s]+=categoryFreqs[i]*transProbs[parentStates[p]*nStates + s]*partials[p*nStates + s];
		   		}
		   	}
		   	double sum=0.0;
		   	for (int s = 0; s < nStates; s++) {
		   		sum+=probs[s];
		   	}
//		   	if(sum<0.0000000001){
//	    		System.out.println("sum: "+sum+" "+parentStates[p]+" "+time+" ");
//	    		for (int t = 0; t<nTypes; t++) {
//	        		//System.out.println("Index t "+t+"\n");
//	        		//System.out.println("pMatrix index "+((t*nTypes) + parentDeme)+"\n");
//	    			System.out.println(t+" "+probs[t]+" ");
//	        	}
//	    	}
	    	for (int t = 0; t<nTypes; t++) {
	    		probs[t]=probs[t]/sum;
	    	}
	    	//Set node type!
	    	//System.out.println("Set node type");
	    	double x = Randomizer.nextDouble();
	    	int type=-1;
	    	sum=0.0;
	    	//System.out.println("probs: ");
	    	for (int i = 0; i<probs.length; i++) {
	    		sum+=probs[i];
	    		//System.out.println(probs[i]);
	    		if (x<sum){
	    			type=i;
	    			break;
	    		}
	    	}
	    	if (type==-1) {
	    		System.out.println("Something is wrong with probabilities.");
	    		System.exit(0);
	    	}
	    	types[p]=type;
	    }
   	 	likelihoodCore.setNodeStates(node.getNr(), types);

    	//Then make iteration
    	if (node.getLeft()!= null){
    		Node LC= node.getLeft();
    			double LCTime= node.getHeight() - LC.getHeight();
    			//System.out.println("launch first iteration\n");
    			setDemeIterative(LC ,LCTime, types);//((MultiTypeNodeVolz) node).getNodeType()
    	}
    	
    	if (node.getRight()!= null){
    		Node RC= node.getRight();
    			double RCTime= node.getHeight() - RC.getHeight();
    			//System.out.println("launch second iteration\n");
    			setDemeIterative(RC ,RCTime, types);
    	}
    	//System.out.println("setDemeIterative end.");
    }
    
    
    
    
    /** Print tree with demes represented as names instead of numbers **/
    public void log(int nSample, PrintStream out) {
    	//System.out.println("logging LK start.");
    	//double prob=lineageProbsList.get(lineageProbsList.size()-1)[0];
        //if (prob==prob){
        	//System.out.println("Set all demes called from log "+prob+"\n");
    	if (likelihoodCore != null) {
    		setAllDemes();
    		out.print("tree STATE_" + nSample + " = ");
    		out.print(toSortedNewickVolz(treeInput.get().getRoot(), new int[1])+";");
    	}
        //loggable=true;
        //}else{ loggable=false;}
        //System.out.println("logging LK end.");
    }
    
    
    
    
    public String toSortedNewickVolz(Node node, int[] iMaxNodeInClade) {
    	//System.out.println("toSortedNewickVolz start.");
        StringBuilder buf = new StringBuilder();
        if (node.getLeft() != null) {
            buf.append("(");
            String sChild1 = toSortedNewickVolz(node.getLeft(), iMaxNodeInClade);
            int iChild1 = iMaxNodeInClade[0];
            if (node.getRight() != null) {
                String sChild2 = toSortedNewickVolz(node.getRight(), iMaxNodeInClade);
                int iChild2 = iMaxNodeInClade[0];
                if (iChild1 > iChild2) {
                    buf.append(sChild2);
                    buf.append(",");
                    buf.append(sChild1);
                } else {
                    buf.append(sChild1);
                    buf.append(",");
                    buf.append(sChild2);
                    iMaxNodeInClade[0] = iChild1;
                }
            } else {
                buf.append(sChild1);
            }
            buf.append(")");
            if (getID() != null) {
                buf.append(node.getNr()+1);
            }
        } else {
            iMaxNodeInClade[0] = node.getNr();
            buf.append(node.getNr() + 1);
        }

        //if (printMetaData) {
        int patternCount = dataInput.get().getPatternCount();
        int[] states= new int[patternCount];
    	likelihoodCore.getNodeStates(node.getNr(), states);
        //int nodeT=nodeTypes[node.getNr()];
        
        String loc;
        loc=Integer.toString(states[0]);
        for(int p=1; p<patternCount;p++){
        	loc+=(","+Integer.toString(states[p]));
        }
        //if (nodeT>(mtTree.getTypeList().size()-1)) loc="Unsampled"+(nodeT-mtTree.getTypeList().size());
        //else loc=mtTree.getTypeList().get(nodeT);
        buf.append(("[&states="+loc+"]"));
        //buf.append(("[&location="+mtTree.getTypeList().get(nodeTypes[((MultiTypeNodeVolz) node).getNr()])+"]"));
        //}
        buf.append(":").append(node.getLength());
        //System.out.println("toSortedNewickVolz end.");
        return buf.toString();
        
    }
    
    
    
    
    
//    /**
//	 takes a vector x of lineage probabilities for the base of the branch.
//	 evaluates y = exp(H^T t) x
//	 where H is the 'tavare' matrix for the coalescent probabilities.
//	 Hence,
//	 y(i) = \sum_j P(i lineages at top of branch | j lineages at bottom) x(j)
//
//	 **/
//	double[] computePoMoProbs(double t,double[] x, PoMoMatrixSparse PMS) throws Exception {
//		double[] y = new double [x.length];
////		int n = 0;
////		for(int i=0;i<x.length;i++)
////			if (x[i]>0.0)
////				n=i;
//		
//		//PoMoMatrixSparse tavMatrix = new PoMoMatrixSparse(n); 
//
//		double [] xcopy = new double[x.length];
//		System.arraycopy(x, 0, xcopy, 0, x.length);
//		double [] ycopy = MatrixExponentiator.expmv(t, PMS, xcopy);
//		System.arraycopy(ycopy, 0, y, 0, x.length);
//		
//		return y;
//	} // computeTavareProbs
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
} // class TreeLikelihoodPoMo
