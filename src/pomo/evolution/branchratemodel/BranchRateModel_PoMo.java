package pomo.evolution.branchratemodel;

import beast.base.inference.CalculationNode;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.inference.parameter.RealParameter;
import beast.base.evolution.tree.Node;

/**
 * @author Alexei Drummond
 */
@Description("Defines a mean rate for each branch in the beast.tree.")
public interface BranchRateModel_PoMo {

    public double getPopSizeForBranch(Node node);

    @Description(value = "Base implementation of a clock model.", isInheritable = false)
    public abstract class Base extends CalculationNode implements BranchRateModel_PoMo {
        final public Input<RealParameter> meanRateInput = new Input<>("realPopSize", "real population size multiplier (defaults to 1.0)");

        // empty at the moment but brings together the required interfaces
    }
}
