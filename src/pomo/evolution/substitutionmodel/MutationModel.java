/*
 * Copyright (C) 2014 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pomo.evolution.substitutionmodel;

import beast.base.inference.CalculationNode;
import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Loggable;
import beast.base.inference.parameter.RealParameter;

import java.io.PrintStream;
//import java.util.ArrayList;
//import java.util.List;

//import org.jblas.DoubleMatrix;
//import org.jblas.MatrixFunctions;


//import org.jblas.DoubleMatrix;
//import org.jblas.ComplexDoubleMatrix;
//import org.jblas.MatrixFunctions;
//import org.jblas.Eigen;


/**
 * @author Nicola De Maio 2015
 */
@Description("Describe the mutation model to be used (GTR, HKY, F81, general) within PoMo")
public class MutationModel extends CalculationNode implements Loggable {

    public Input<RealParameter> rateVectorInput = new Input<RealParameter>(
            "rateVector",
            "Mutation rate vector, any lenght among 1, 2, 6, 12 depending on the model that one wants to use. in case of 12, nucleotide frequencies are not used.",
            Validate.REQUIRED);
    public Input<RealParameter> nucFreqsInput = new Input<RealParameter>(
            "nucFreqs",
            "nucleotide frequencies as A, C, G, T.",
            Validate.REQUIRED);
    
    private RealParameter rateVector, nucFreqs;
    private enum MutKind {
        F81, HKY, GTR, General
    };
    private MutKind mutKind;
    
    

    @Override
    public void initAndValidate() throws IllegalArgumentException {
        
        rateVector = rateVectorInput.get();
        nucFreqs = nucFreqsInput.get();
        
        rateVector.setLower(0.0);
        nucFreqs.setLower(0.0);
        
        if (rateVector.getDimension() == 1) {
            mutKind = MutKind.F81;
        }else if (rateVector.getDimension() == 2) {
        	mutKind = MutKind.HKY;
        }else if (rateVector.getDimension() == 6) {
        	mutKind = MutKind.GTR;
        }else if (rateVector.getDimension() == 12) {
        	mutKind = MutKind.General;
        } else {
            throw new IllegalArgumentException("Mutation rates vector has incorrect number of elements (only 1=F81, 2=HKY, 6=GTR, and 12=General allowed for now).");
        }
        
        if (nucFreqs.getDimension() != 4) {
        	throw new IllegalArgumentException("4 nucleotide frequencies must be provided.");
        }
        
    }
    
    
    /**
     * @return scaled mutation rates. Order: A->C, A->G, A->T, C->A, etc...
     */
    public Double[] getMutRates() {
    	Double[] rates = new Double[12] ;
    	Double[] values= getMutValues() ;
        Double[] freqs= getFreqs();
        
        switch (mutKind) {
	        case F81:
	        	rates[0]=freqs[1]*values[0];
	        	rates[1]=freqs[2]*values[0];
	        	rates[2]=freqs[3]*values[0];
	        	rates[3]=freqs[0]*values[0];
	        	rates[4]=freqs[2]*values[0];
	        	rates[5]=freqs[3]*values[0];
	        	rates[6]=freqs[0]*values[0];
	        	rates[7]=freqs[1]*values[0];
	        	rates[8]=freqs[3]*values[0];
	        	rates[9]=freqs[0]*values[0];
	        	rates[10]=freqs[1]*values[0];
	        	rates[11]=freqs[2]*values[0];
	            break;
	        case HKY:
	        	rates[0]=freqs[1]*values[0];
	        	rates[1]=freqs[2]*values[1];
	        	rates[2]=freqs[3]*values[0];
	        	rates[3]=freqs[0]*values[0];
	        	rates[4]=freqs[2]*values[0];
	        	rates[5]=freqs[3]*values[1];
	        	rates[6]=freqs[0]*values[1];
	        	rates[7]=freqs[1]*values[0];
	        	rates[8]=freqs[3]*values[0];
	        	rates[9]=freqs[0]*values[0];
	        	rates[10]=freqs[1]*values[1];
	        	rates[11]=freqs[2]*values[0];
	            break;
	        case GTR:
	        	rates[0]=freqs[1]*values[0];
	        	rates[1]=freqs[2]*values[1];
	        	rates[2]=freqs[3]*values[2];
	        	rates[3]=freqs[0]*values[0];
	        	rates[4]=freqs[2]*values[3];
	        	rates[5]=freqs[3]*values[4];
	        	rates[6]=freqs[0]*values[1];
	        	rates[7]=freqs[1]*values[3];
	        	rates[8]=freqs[3]*values[5];
	        	rates[9]=freqs[0]*values[2];
	        	rates[10]=freqs[1]*values[4];
	        	rates[11]=freqs[2]*values[5];
	            break;
	        case General:
	        	for (int i=0;i<12;i++) rates[i]=values[i];
	            break;
        }
        return rates;
    } // getTransitionProbabilities
    


    /**
     * @return nucleotide frequencies.
     */
    public Double[] getFreqs() {
    	Double[] freqs=nucFreqs.getValues() ;
    	double sum=0.0;
    	for (int i=0; i<4; i++) sum+=freqs[i];
    	for (int i=0; i<4; i++) freqs[i]=freqs[i]/sum;
        return freqs ;
    }
    
    /**
     * @return non-scaled mutation rates.
     */
    public Double[] getMutValues() {
        return rateVector.getValues() ;
    }
    
    

    /*
     * Methods implementing loggable interface
     */
    
    @Override
    public void init(PrintStream out) throws IllegalArgumentException {
        String outName;
        if (getID() == null || getID().matches("\\s*"))
            outName = "mutModel";
        else
            outName = getID();
        for (int i=0; i<rateVector.getDimension(); i++) {
            out.print(outName + ".mutRateRaw_" + i + "\t");
        }
        for (int i=0; i<4; i++) {
            out.print(outName + ".freq_" + i + "\t");
        }
    }

    @Override
    public void log(long nSample, PrintStream out) {
    	Double[] values= getMutValues() ;
        Double[] freqs= getFreqs();
    	for (int i=0; i<values.length ; i++) {
            out.print(values[i] + "\t");
        }
    	for (int i=0; i<4 ; i++) {
            out.print(freqs[i] + "\t");
        }
    }


	@Override
	public void close(PrintStream out) {
	}
	
    @Override
    protected boolean requiresRecalculation() {
        return false;
    }

    @Override
    protected void restore() {
        super.restore();
    }
    
    public static void main (String [] args) {
    }

}
