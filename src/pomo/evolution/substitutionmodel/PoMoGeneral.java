/*
* File GeneralSubstitutionModel.java
*
* Author 2015 Nicola De Maio
*/
package pomo.evolution.substitutionmodel;


import java.lang.reflect.Constructor;

import beast.base.evolution.substitutionmodel.DefaultEigenSystem;
import beast.base.evolution.substitutionmodel.EigenDecomposition;
import beast.base.evolution.substitutionmodel.EigenSystem;
import beast.base.evolution.substitutionmodel.SubstitutionModel;
import beast.base.inference.CalculationNode;
import beast.base.core.Citation;
import beast.base.core.Description;
//import beast.base.core.Function;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.inference.parameter.RealParameter;
import beast.base.evolution.datatype.DataType;
import beast.base.evolution.tree.Node;




@Description("Specifies transition probability matrix for PoMo. Not in general reversible, unless fitnesses are equal and root frequencies are not estimated.")
@Citation("De Maio, Schloetterer and Kosiol (2013). Linking great apes genome evolution across time scales using polymorphism-aware phylogenetic models. Molecular Biology and Evolution 30(10), 2249-2262. \n\n"+
" De Maio, Schrempf and Kosiol (2015). PoMo: An Allele Frequency-Based Approach for Species Tree Estimation.  Systematic Biology doi: 10.1093/sysbio/syv048. ")
public class PoMoGeneral extends CalculationNode implements SubstitutionModel {
	
    public Input<MutationModel> mutationModelInput =  new Input<MutationModel>("mutModel", "the mutation model. ", Validate.REQUIRED);

    public Input<String> eigenSystemClass = new Input<String>("eigenSystem", "Name of the class used for creating an EigenSystem", DefaultEigenSystem.class.getName());

	public Input<RealParameter> fitnessInput = new Input<RealParameter>("fitness", "parameter which defines the fitness vector for different alleles. In case of neutrality, specify equal fitness for all alleles. Length 4 required. ", Validate.REQUIRED);
	
	public Input<Integer> virtualPopInput = new Input<Integer>("virtualPop", "parameter which defines the virtual population number of individuals. Determines the number of polymorphic states for each pair of alleles. ", Validate.REQUIRED);
	
	public Input<Double> thetaInput = new Input<Double>("theta", "parameter describing how much within-host variation is expected", 0.01);
	
	public Input<Boolean> useThetaInput = new Input<Boolean>("useTheta", "boolean parameter describing if to use the input value of theta or if to ignore it", false);
	
	//public Input<RealParameter> seqErrorInput = new Input<RealParameter>("seqError", " starting sequencing error rate, 0 by default", Validate.REQUIRED);
	
	public Input<RealParameter> rootNucFreqsInput = new Input<RealParameter>("rootNucFreqs", "Root nucleotide frequencies", Validate.REQUIRED);
	
	public Input<Boolean> estimateFreqs = new Input<Boolean>("estimateRootFreqs", "Should the root nucleotide frequencies be estimated (true)?",false, Validate.REQUIRED);
	
    /**
     * a square m_nStates x m_nStates matrix containing current rates  *
     */
    protected double[][] rateMatrix;
    //protected double[][] rateMatrixBottleneck;
	public int vPop;
	protected MutationModel mutM;
	protected RealParameter fit, rootNucFreqs;
	protected int nrOfStates;
	protected boolean useTheta, estFreqs;
	//protected double bottleneck, storedBottleneck;
	//Double[] fits, fitsStored;
	//double[] frequencies, frequenciesStored;
	//double[] rootFrequencies, rootFrequenciesStored;
	double theta;
	//RealParameter seqErr;
	//double sErr, sErrStored;
	//double bottleneckStrength;
	
	


    @Override
    public void initAndValidate() throws IllegalArgumentException {
        //super.initAndValidate();/*Is this doing what it should?*/
        useTheta = useThetaInput.get();
        theta=thetaInput.get();
        //bottleneckStrength=bottleneckInput.get();
        updateMatrix = true;
        //updateMatrixBottleneck = true;
		mutM = mutationModelInput.get();
		vPop = virtualPopInput.get();
		//seqErr = seqErrorInput.get();
		//sErr = seqErr.getValue();
		//sErrStored = sErr;
		fit=fitnessInput.get();
		//fits=getFit();
		estFreqs=estimateFreqs.get();
		rootNucFreqs=rootNucFreqsInput.get();
		//rootFrequencies=rootFreqs.getValues();
	    //nrOfStates = frequencies.getFreqs().length;
		if ( rootNucFreqs.getValues().length != 4 ) {
	        throw new IllegalArgumentException("Dimension of root nuc freqs is wrong (!=4).");
	    }
	    if ( fit.getValues().length != 4 ) {
	        throw new IllegalArgumentException("Dimension of fitness coefficients is wrong (!=4).");
	    }
	    mutM.initAndValidate();
	    Double[] mutRates=mutM.getMutRates();
	    //Double[] nucFreqs=mutM.getFreqs();
	    //if (useTheta){
	    //	mutRates=applyTheta(vPop, mutRates, theta);
	    //}
	    nrOfStates=6*(vPop-1) +4;
	    rateMatrix = new double[nrOfStates][nrOfStates];
	    rateMatrix = getRates(mutRates, vPop, fit.getValues());
	    //rateMatrixBottleneck = new double[nrOfStates][nrOfStates];
	    //rateMatrixBottleneck = getRatesBottleneck(vPop);
	    
//	    frequencies=new double[nrOfStates];
//	    rootFrequencies=new double[nrOfStates];
//	    frequencies=getFreqs(nucFreqs,mutRates,vPop);
//	    if(estFreqs){
//        	//rootFrequencies=rootFreqs.getValues();
//	    	rootFrequencies=getFreqs(rootNucFreqs.getValues(),mutRates,vPop);
//        	//frequencies=getFreqs(rootNucFreqs.getValues(),mutRates,vPop);
//        }else{
//        	rootFrequencies=getFreqs(nucFreqs,mutRates,vPop);
//        	//frequencies=getFreqs(nucFreqs,mutRates,vPop);
//        }
//	    if (useTheta){
//	    	//frequencies=applyThetaFreqs(vPop, frequencies, theta);
//	    	rootFrequencies=applyThetaFreqs(vPop, rootFrequencies, theta);
//	    }
//	    frequenciesStored = frequencies.clone(); 
//		rootFrequenciesStored = rootFrequencies.clone();
	    
	    try {
			eigenSystem = createEigenSystem();
		} catch (Exception e) {
			e.printStackTrace();
		}
	    //eigenSystem = new DefaultEigenSystem(m_nStates);
	
    } // initAndValidate
    
    //normalizing mutation rates by theta
//    public Double[] applyTheta(Integer v, Double[] mutRates, double theta){
//    	Double[] w;
//    	w = new Double[12];
//    	double norm=0.0;
//    	for (int i=0;i<12;i++)norm+=mutRates[i]*rootFrequencies[i/3];
//    	norm=norm*watterson(vPop);
//    	for (int i=0;i<12;i++)w[i]=mutRates[i]*theta/norm;
//		return w;
//	}
    
  //correct root frequencies by theta
    public double[] applyThetaFreqs(Integer v, double[] freqs, double theta){
    	double varF=0.0;
    	double[] w = new double[freqs.length];
    	for (int i=4;i<freqs.length;i++) varF+=freqs[i];
    	for (int i=4;i<freqs.length;i++) w[i]+=freqs[i]*theta*watterson(v)/varF;
    	for (int i=0;i<4;i++) w[i]+=freqs[i]*(1.0-theta*watterson(v))/(1.0-varF);
		return w;
	}
    
    //Watterson coefficient, the sum up to i=N-1 of 1/i
    public double watterson(Integer v){
    	double w=0.0;
    	for (int i=1; i<v;i++) w+=1.0/i;
		return w;
	}
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //calculate neutral equilibrium frequencies from Dominik
    //
    public double[] getFreqs(Double[] nucFreqs,Double[] mutRates,int vPop){
    	double[] f;
    	f=new double[nrOfStates];
    	//calculate normlizing factor norm
    	double norm=0.0;
    	int i,j,j2,k,j3;
    	double aN=watterson(vPop);
    	for(i=0;i<4;i++){
    		for(j=0;j<4;j++){
    			if (j>i) j2=j-1; else j2=j;
    			if (i!=j) norm+=aN*nucFreqs[i]*mutRates[i*3+j2];
    		}
    	}
    	//calculate frequencies
    	for(i=0;i<4;i++){
    		f[i]=nucFreqs[i]*(1.0-norm);
    	}
    	j3=0;
    	for(i=0;i<4;i++){
    		for(j=0;j<4;j++){
    			if (j>i){
    				for(k=0;k<vPop-1;k++){
    					f[4+j3*(vPop-1)+k]=(nucFreqs[j]*mutRates[j*3+i]/(k+1.0))+(nucFreqs[i]*mutRates[i*3+j-1]/((vPop-k)-1.0));
    				}
    				j3+=1;
    			}
    		}
    	}
    	//String  s="";
    	//double sum=0.0;
    	//for(i=0;i<nrOfStates;i++) {
    	//	s+=(f[i]+", ");
    	//	sum+=f[i];
    	//}
    	//System.out.println("Frequencies: "+s);
    	//System.out.println("sum: "+sum+"\n");
    	return f;
    }
    
    //help with indeces
    public Integer opposite(Integer v){
		if (v==2) return 3;
		else if (v==3) return 2;
		else return v;
	}
    
  //return alleles associate with group
    public static int[] indeces(int v){
    	int[] ind;
    	ind = new int[2];
		if (v==0) {ind[0]=0; ind[1]=1;}
		if (v==1) {ind[0]=0; ind[1]=2;}
		if (v==2) {ind[0]=0; ind[1]=3;}
		if (v==3) {ind[0]=1; ind[1]=2;}
		if (v==4) {ind[0]=1; ind[1]=3;}
		if (v==5) {ind[0]=2; ind[1]=3;}
		return ind;
	}
    
    //PoMo allele frequency change rate
    public double coeff(double fit1,double fit2, int vPop, int num1, int num2){
    	double co=0.0;
    	if (num2==num1+1){
    		co=num1*(1.0+fit1-fit2)*(vPop-num1)/(vPop*(num1*(1.0+fit1-fit2) + (vPop-num1)));
    	}else if (num2==num1-1){
    		co=num1*(vPop-num1)/(vPop*(num1*(1.0+fit1-fit2) + (vPop-num1)));
    	}else {
    		System.out.println("Problem with PoMo indeces in calculating the rate matrix");
    		System.exit(0);
    	}
    	return co;
    }
    
    
    /**
     * get the PoMo rate Matrix from mutation rates and fitness coefficients.
     * First 4 rows are fixed states, ordered A,C,G,T. Then are polymorhic state group, ordered A->C, A->G, A->T, C->G, C->T, G->T. 
     * Within each group states are ordered 1A-9C, 2A-8C, etc... 
     */
    public double[][] getRates(Double[] mutRates, int vPop, Double[] fits){
    	int nrOfStates=6*(vPop-1) +4;
    	double[][] rates;
    	rates= new double[nrOfStates][nrOfStates];
    	int i,j,k,k2;
    	for(i=0;i<nrOfStates;i++){
    		for(j=0;j<nrOfStates;j++) rates[i][j]=0.0;
    	}
    	
    	//mutation rates
    	k=0;
    	k2=0;
    	for(i=0;i<4;i++){
    		for(j=0;j<4;j++){
    			if (i<j){
    				rates[i][4+k*(vPop-1)+(vPop-2)]=mutRates[k+k2];
    				k+=1;
    			}
    			if (i>j){
    				rates[i][4+opposite(k2)*(vPop-1)]=mutRates[k+k2];
    				k2+=1;
    			}
    		}
    	}
    	
    	//fixation rates
    	k=0;
    	k2=0;
    	for(i=0;i<4;i++){
    		for(j=0;j<4;j++){
    			if (i<j){
    				rates[4+k*(vPop-1)][j]=coeff(fits[j],fits[i], vPop, 1, 0)*vPop;
    				rates[4+k*(vPop-1)+(vPop-2)][i]=coeff(fits[i],fits[j], vPop, 1, 0)*vPop;
    				k+=1;
    			}
    		}	
    	}
    	
    	//other allele frequency changes
    	for(i=0;i<6;i++){
    		int[] ind=indeces(i);
    		for(j=0;j<vPop-1;j++){
    			if (j>0) rates[4+i*(vPop-1)+j][4+i*(vPop-1)+j-1]=coeff(fits[ind[0]],fits[ind[1]], vPop, j+1, j)*vPop;
    			if (j<vPop-2) rates[4+i*(vPop-1)+j][4+i*(vPop-1)+j+1]=coeff(fits[ind[0]],fits[ind[1]], vPop, j+1, j+2)*vPop;
    		}
    	}
    	
    	//diagonal elements
    	for(i=0;i<nrOfStates;i++){
    		double sum=0.0;
    		for(j=0;j<nrOfStates;j++){
    			if (i!=j)sum+=rates[i][j];
    		}
    		rates[i][i]=-sum;
    	}
    	return rates;
    }
    
    
    
    
//    /**
//     * get the PoMo rate Matrix for a population bottleneck.
//     * First 4 rows are fixed states, ordered A,C,G,T. Then are polymorhic state group, ordered A->C, A->G, A->T, C->G, C->T, G->T. 
//     * Within each group states are ordered 1A-9C, 2A-8C, etc... 
//     */
//    public double[][] getRatesBottleneck( int vPop){
//    	int nrOfStates=6*(vPop-1) +4;
//    	double[][] rates;
//    	rates= new double[nrOfStates][nrOfStates];
//    	int i,j,k;//,k2;
//    	for(i=0;i<nrOfStates;i++){
//    		for(j=0;j<nrOfStates;j++) rates[i][j]=0.0;
//    	}
//    	
//    	//fixation rates
//    	k=0;
//    	//k2=0;
//    	for(i=0;i<4;i++){
//    		for(j=0;j<4;j++){
//    			if (i<j){
//    				rates[4+k*(vPop-1)][j]=coeff(0.0,0.0, vPop, 1, 0);
//    				rates[4+k*(vPop-1)+(vPop-2)][i]=coeff(0.0,0.0, vPop, 1, 0);
//    				k+=1;
//    			}
//    		}	
//    	}
//    	
//    	//other allele frequency changes
//    	for(i=0;i<6;i++){
//    		//int[] ind=indeces(i);
//    		for(j=0;j<vPop-1;j++){
//    			if (j>0) rates[4+i*(vPop-1)+j][4+i*(vPop-1)+j-1]=coeff(0.0,0.0, vPop, j+1, j);
//    			if (j<vPop-2) rates[4+i*(vPop-1)+j][4+i*(vPop-1)+j+1]=coeff(0.0,0.0, vPop, j+1, j+2);
//    		}
//    	}
//    	
//    	//diagonal elements
//    	for(i=0;i<nrOfStates;i++){
//    		double sum=0.0;
//    		for(j=0;j<nrOfStates;j++){
//    			if (i!=j)sum+=rates[i][j];
//    		}
//    		rates[i][i]=-sum;
//    	}
//    	return rates;
//    }
    
    
    
    
    

    /**
     * create an EigenSystem of the class indicated by the eigenSystemClass input *
     */
    protected EigenSystem createEigenSystem() throws Exception {
        Constructor<?>[] ctors = Class.forName(eigenSystemClass.get()).getDeclaredConstructors();
        Constructor<?> ctor = null;
        for (int i = 0; i < ctors.length; i++) {
            ctor = ctors[i];
            if (ctor.getGenericParameterTypes().length == 1)
                break;
        }
        ctor.setAccessible(true);
        return (EigenSystem) ctor.newInstance(nrOfStates);
    }

    protected double[] relativeRates;
    protected double[] storedRelativeRates;

    protected EigenSystem eigenSystem;
   //protected EigenSystem eigenSystemBottleneck;

    protected EigenDecomposition eigenDecomposition;
    private EigenDecomposition storedEigenDecomposition;
    
//    protected EigenDecomposition eigenDecompositionBottleneck;
//    private EigenDecomposition storedEigenDecompositionBottleneck;

    protected boolean updateMatrix = true;
    private boolean storedUpdateMatrix = true;
    
    //protected boolean updateMatrixBottleneck = true;
    //private boolean storedUpdateMatrixBottleneck = true;
    
    
    
    
    /**
     * @return fitness coefficients.
     */
    public Double[] getFit() {
        return fit.getValues() ;
    }
    
    /**
     * access to (copy of) rate matrix *
     */
    protected double[][] getRateMatrix() {
        return rateMatrix.clone();
    }
    
//    /**
//     * access to (copy of) rate matrix for the bottleneck *
//     */
//    protected double[][] getRateMatrixBottleneck() {
//        return rateMatrixBottleneck.clone();
//    }
    
    public double[] getRateMatrix(Node node) {
        return null;
    }

	/*** access to popSize **/
    public int getPopSize() {
        return vPop;
    }
    
//    /*** access to sequencing error **/
//    public double getSErr() {
//        return sErr;
//    }
    
//    /*** access to stored sequencing error **/
//    public double getStoredSErr() {
//        return sErrStored;
//    }

	/*** access to nrOfAlleles **/
    protected int getNrOfAlleles() {
        return 4;
    }
    
    /*** access to nrOfStates **/
    protected int getNrOfStates() {
        return nrOfStates;
    }
    
    /*** get root frequencies ***/
    public double[] getFrequencies() {
    	Double[] mutRates=mutM.getMutRates();
	    Double[] nucFreqs=mutM.getFreqs();
    	double[] rootFrequencies=new double[nrOfStates];
	    if(estFreqs){
        	//rootFrequencies=rootFreqs.getValues();
	    	rootFrequencies=getFreqs(rootNucFreqs.getValues(),mutRates,vPop);
        	//frequencies=getFreqs(rootNucFreqs.getValues(),mutRates,vPop);
        }else{
        	rootFrequencies=getFreqs(nucFreqs,mutRates,vPop);
        	//frequencies=getFreqs(nucFreqs,mutRates,vPop);
        }
	    if (useTheta){
	    	//frequencies=applyThetaFreqs(vPop, frequencies, theta);
	    	rootFrequencies=applyThetaFreqs(vPop, rootFrequencies, theta);
	    }
    	return rootFrequencies;
    	//Double[] mutRates=mutM.getMutRates();
    	//return getFreqs(rootFrequencies,mutRates,vPop);
    }
    
    /*** get root nuc frequencies ***/
    public Double[] getNucFrequencies() {
	    if(estFreqs){
	    	return rootNucFreqs.getValues();
        }else{
        	return mutM.getFreqs();
        }
    }
    
    /*** get root nuc mutation rates ***/
    public Double[] getNucMuts() {
    	return mutM.getMutRates();
    }
    
    /*** number of states in PoMo ***/
    public int getStateCount() {
        return nrOfStates;
    }
    
    public boolean canReturnComplexDiagonalization() {
        return false;
    }
    
    
    public void scaleRateMatrix(double[][] newRateMatrix, double[][] rateMatrix, double fRate, double popSize){
    	int i,j,k,k2;
    	for(i=0;i<nrOfStates;i++){
    		for(j=0;j<nrOfStates;j++) newRateMatrix[i][j]=0.0;
    	}
    	
    	//mutation rates
    	k=0;
    	k2=0;
    	for(i=0;i<4;i++){
    		for(j=0;j<4;j++){
    			if (i<j){
    				newRateMatrix[i][4+k*(vPop-1)+(vPop-2)]=rateMatrix[i][4+k*(vPop-1)+(vPop-2)]*fRate;
    				k+=1;
    			}
    			if (i>j){
    				newRateMatrix[i][4+opposite(k2)*(vPop-1)]=rateMatrix[i][4+opposite(k2)*(vPop-1)]*fRate;
    				k2+=1;
    			}
    		}
    	}
    	
    	//fixation rates
    	k=0;
    	k2=0;
    	for(i=0;i<4;i++){
    		for(j=0;j<4;j++){
    			if (i<j){
    				newRateMatrix[4+k*(vPop-1)][j]=rateMatrix[4+k*(vPop-1)][j]/popSize;
    				newRateMatrix[4+k*(vPop-1)+(vPop-2)][i]=rateMatrix[4+k*(vPop-1)+(vPop-2)][i]/popSize;
    				k+=1;
    			}
    		}	
    	}
    	
    	//other allele frequency changes
    	for(i=0;i<6;i++){
    		for(j=0;j<vPop-1;j++){
    			if (j>0) newRateMatrix[4+i*(vPop-1)+j][4+i*(vPop-1)+j-1]=rateMatrix[4+i*(vPop-1)+j][4+i*(vPop-1)+j-1]/popSize;
    			if (j<vPop-2) newRateMatrix[4+i*(vPop-1)+j][4+i*(vPop-1)+j+1]=rateMatrix[4+i*(vPop-1)+j][4+i*(vPop-1)+j+1]/popSize;
    		}
    	}
    	
    	//diagonal elements
    	for(i=0;i<nrOfStates;i++){
    		double sum=0.0;
    		for(j=0;j<nrOfStates;j++){
    			if (i!=j)sum+=newRateMatrix[i][j];
    		}
    		newRateMatrix[i][i]=-sum;
    	}
    }
    
    
    public void getTransitionProbabilities(double branchTime, double fRate, double[] matrix, double popSize){
    	double distance = branchTime;
    	int i, j, k;
        double temp;
        
        

        // this must be synchronized to avoid being called simultaneously by
        // two different likelihood threads - AJD
        synchronized (this) {
            if (updateMatrix) {
                //setupRelativeRates();
                setupRateMatrix();
                double[][] newRateMatrix = new double[nrOfStates][nrOfStates];
                scaleRateMatrix(newRateMatrix, rateMatrix, fRate, popSize);
                eigenDecomposition = eigenSystem.decomposeMatrix(newRateMatrix);
                //eigenDecompositionBottleneck = eigenSystem.decomposeMatrix(rateMatrixBottleneck);
                updateMatrix = false;
            }
        }

        // is the following really necessary?
        // implemented a pool of iexp matrices to support multiple threads
        // without creating a new matrix each call. - AJD
        // a quick timing experiment shows no difference - RRB
        double[] iexp = new double[nrOfStates * nrOfStates];
        // Eigen vectors
        double[] Evec = eigenDecomposition.getEigenVectors();
        // inverse Eigen vectors
        double[] Ievc = eigenDecomposition.getInverseEigenVectors();
        // Eigen values
        double[] Eval = eigenDecomposition.getEigenValues();
        for (i = 0; i < nrOfStates; i++) {
            temp = Math.exp(distance * Eval[i]);
            for (j = 0; j < nrOfStates; j++) {
                iexp[i * nrOfStates + j] = Ievc[i * nrOfStates + j] * temp;
            }
        }

        int u = 0;
        for (i = 0; i < nrOfStates; i++) {
            for (j = 0; j < nrOfStates; j++) {
                temp = 0.0;
                for (k = 0; k < nrOfStates; k++) {
                    temp += Evec[i * nrOfStates + k] * iexp[k * nrOfStates + j];
                }

                matrix[u] = Math.abs(temp);
                u++;
            }
        }
        
        //if exponentiation fails, return matrix of 0s to give -infinite log-likelihood
        for (i = 0; i < nrOfStates; i++) {
        	temp = 0.0;
            for (j = 0; j < nrOfStates; j++) {
                temp +=matrix[i*nrOfStates +j];
            }
            if (temp>1.001 || temp<0.999){
            	//System.out.println("Matrix exponentiation has given non-probability matrix: rejecting move\n");
            	for (i = 0; i < nrOfStates; i++) {
	            	for (j = 0; j < nrOfStates; j++) {
	                    matrix[i*nrOfStates +j]=0.0;
	                }
            	}
            	break;
            }	
        }
    }

    @Override
    public void getTransitionProbabilities(Node node, double fStartTime, double fEndTime, double fRate, double[] matrix) {
    	getTransitionProbabilities(fStartTime - fEndTime, fRate, matrix, 1.0);
    } // getTransitionProbabilities

    public void getTransitionProbabilities(Node node, double fStartTime, double fEndTime, double fRate, double[] matrix, double popSize) {
    	getTransitionProbabilities(fStartTime - fEndTime, fRate, matrix, popSize);
    } // getTransitionProbabilities
    
//    public double getBottleneckSize(){
//    	return bottle.getValue();
//    }
    
    
//    public void getTransitionProbabilitiesBottleneck(double[] matrix, double bottleSize) {
//        //double distance = bottle.getValue();
//        
//        int i, j, k;
//        double temp;
//
//        // this must be synchronized to avoid being called simultaneously by
//        // two different likelihood threads - AJD
//        if(bottleSize!=bottleneck){
//        	
//        	setupRateMatrixBottleneck();
//        	eigenDecompositionBottleneck = eigenSystem.decomposeMatrix(rateMatrixBottleneck);
//        }
//        synchronized (this) {
//            if (updateMatrixBottleneck) {
//                //setupRelativeRates();
//                setupRateMatrixBottleneck();
//                //eigenDecomposition = eigenSystem.decomposeMatrix(rateMatrix);
//                eigenDecompositionBottleneck = eigenSystem.decomposeMatrix(rateMatrixBottleneck);
//                updateMatrixBottleneck = false;
//            }
//        }
//
//        // is the following really necessary?
//        // implemented a pool of iexp matrices to support multiple threads
//        // without creating a new matrix each call. - AJD
//        // a quick timing experiment shows no difference - RRB
//        double[] iexp = new double[nrOfStates * nrOfStates];
//        // Eigen vectors
//        double[] Evec = eigenDecompositionBottleneck.getEigenVectors();
//        // inverse Eigen vectors
//        double[] Ievc = eigenDecompositionBottleneck.getInverseEigenVectors();
//        // Eigen values
//        double[] Eval = eigenDecompositionBottleneck.getEigenValues();
//        for (i = 0; i < nrOfStates; i++) {
//            temp = Math.exp(bottleSize * Eval[i]);
//            for (j = 0; j < nrOfStates; j++) {
//                iexp[i * nrOfStates + j] = Ievc[i * nrOfStates + j] * temp;
//            }
//        }
//
//        int u = 0;
//        for (i = 0; i < nrOfStates; i++) {
//            for (j = 0; j < nrOfStates; j++) {
//                temp = 0.0;
//                for (k = 0; k < nrOfStates; k++) {
//                    temp += Evec[i * nrOfStates + k] * iexp[k * nrOfStates + j];
//                }
//
//                matrix[u] = Math.abs(temp);
//                u++;
//            }
//        }
//        
//        //if exponentiation fails, return matrix of 0s to give -infinite log-likelihood
//        for (i = 0; i < nrOfStates; i++) {
//        	temp = 0.0;
//            for (j = 0; j < nrOfStates; j++) {
//                temp +=matrix[i*nrOfStates +j];
//            }
//            if (temp>1.001 || temp<0.999){
//            	//System.out.println("Matrix exponentiation has given non-probability matrix: rejecting move\n");
//            	for (i = 0; i < nrOfStates; i++) {
//	            	for (j = 0; j < nrOfStates; j++) {
//	                    matrix[i*nrOfStates +j]=0.0;
//	                }
//            	}
//            	break;
//            }	
//        }
//    } // getTransitionProbabilities
    
    
    
    
    
    

    /**
     * sets up rate matrix *
     */
    protected void setupRateMatrix() {
    	//sErr = seqErr.getValue();
    	mutM = mutationModelInput.get();
		vPop = virtualPopInput.get();
		//fits=getFit();
		Double[] mutRates=mutM.getMutRates();
	    Double[] nucFreqs=mutM.getFreqs();
	    //if (useTheta){
	    //	mutRates=applyTheta(vPop, mutRates, theta);
	    //}
	    
    	rateMatrix=getRates(mutRates, vPop, fit.getValues());
    	//rateMatrixBottleneck=getRatesBottleneck(mutRates, vPop, fits);
    	
        //double[] fFreqs = frequencies.getFreqs();
    	//frequencies=getFreqs(mutM.getFreqs(),mutM.getMutRates(),vPop);
    	double[] frequencies=new double[nrOfStates];
        frequencies=getFreqs(nucFreqs,mutRates,vPop);
        // normalise rate matrix to one expected substitution per unit time
        double fSubst = 0.0;
        for (int i = 0; i < nrOfStates; i++)
            fSubst += -rateMatrix[i][i] * frequencies[i];

        for (int i = 0; i < nrOfStates; i++) {
            for (int j = 0; j < nrOfStates; j++) {
                rateMatrix[i][j] = rateMatrix[i][j] / fSubst;
            }
        }
//        if(estFreqs){
//        	//rootFrequencies=rootFreqs.getValues();
//	    	rootFrequencies=getFreqs(rootNucFreqs.getValues(),mutRates,vPop);
//        	//frequencies=getFreqs(rootNucFreqs.getValues(),mutRates,vPop);
//        }else{
//        	rootFrequencies=getFreqs(nucFreqs,mutRates,vPop);
//        	//frequencies=getFreqs(nucFreqs,mutRates,vPop);
//        }
//	    if (useTheta){
//	    	//frequencies=applyThetaFreqs(vPop, frequencies, theta);
//	    	rootFrequencies=applyThetaFreqs(vPop, rootFrequencies, theta);
//	    }
        
    } // setupRateMatrix
    
    
    
//    /**
//     * sets up rate matrix for the bottleneck*
//     */
//    protected void setupRateMatrixBottleneck() {
//		vPop = virtualPopInput.get();
//	    //Double[] nucFreqs=mutM.getFreqs();
//    	rateMatrixBottleneck=getRatesBottleneck(vPop);   
//    }
    


    /**
     * CalculationNode implementation follows *
     */
    @Override
    public void store() {
    	//sErrStored=sErr;
        storedUpdateMatrix = updateMatrix ;
        //storedUpdateMatrixBottleneck = updateMatrixBottleneck ;
        if( eigenDecomposition != null ) {
            storedEigenDecomposition = eigenDecomposition.copy();
        }
//        if( eigenDecompositionBottleneck != null ) {
//            storedEigenDecompositionBottleneck = eigenDecompositionBottleneck.copy();
//        }
        
        //fitsStored=fits.clone();
        //frequenciesStored = frequencies.clone(); 
		//rootFrequenciesStored = rootFrequencies.clone();
//        System.arraycopy(relativeRates, 0, storedRelativeRates, 0, relativeRates.length);

        super.store();
    }

    /**
     * Restore the additional stored state
     */
    @Override
    public void restore() {
    	//sErr=sErrStored;
        updateMatrix = storedUpdateMatrix;
        //updateMatrixBottleneck = storedUpdateMatrixBottleneck;
        //frequencies = frequenciesStored.clone(); 
		//rootFrequencies = rootFrequenciesStored.clone();
		//fits=fitsStored.clone();

        // To restore all this stuff just swap the pointers...
//        double[] tmp1 = storedRelativeRates;
//        storedRelativeRates = relativeRates;
//        relativeRates = tmp1;
        if( storedEigenDecomposition != null ) {
            EigenDecomposition tmp = storedEigenDecomposition;
            storedEigenDecomposition = eigenDecomposition;
            eigenDecomposition = tmp;
        }
//        if( storedEigenDecompositionBottleneck != null ) {
//            EigenDecomposition tmp = storedEigenDecompositionBottleneck;
//            storedEigenDecompositionBottleneck = eigenDecompositionBottleneck;
//            eigenDecompositionBottleneck = tmp;
//        }
        super.restore();

    }

    @Override
    protected boolean requiresRecalculation() {
        // we only get here if something is dirty
        updateMatrix = true;
        //updateMatrixBottleneck = true;
        return true;
    }


    /**
     * This function returns the Eigen vectors.
     *
     * @return the array
     */
    @Override
    public EigenDecomposition getEigenDecomposition(Node node) {
        return getEigenDecomposition();
    }
    
    public EigenDecomposition getEigenDecomposition() {
        synchronized (this) {
            if (updateMatrix) {
                //setupRelativeRates();
                setupRateMatrix();
                eigenDecomposition = eigenSystem.decomposeMatrix(rateMatrix);
                //eigenDecompositionBottleneck = eigenSystem.decomposeMatrix(rateMatrixBottleneck);
                updateMatrix = false;
            }
        }
        return eigenDecomposition;
    }
    
//    /**
//     * This function returns the Eigen vectors for the bottlenecks.
//     *
//     * @return the array
//     */
//    public EigenDecomposition getEigenDecompositionBottlenecks(Node node) {
//        return getEigenDecompositionBottlenecks();
//    }
//    
//    public EigenDecomposition getEigenDecompositionBottlenecks() {
//        synchronized (this) {
//            if (updateMatrixBottleneck) {
//                //setupRelativeRates();
//                setupRateMatrixBottleneck();
//                //eigenDecomposition = eigenSystem.decomposeMatrix(rateMatrix);
//                eigenDecompositionBottleneck = eigenSystem.decomposeMatrix(rateMatrixBottleneck);
//                updateMatrixBottleneck = false;
//            }
//        }
//        return eigenDecompositionBottleneck;
//    }

    @Override
    public boolean canHandleDataType(DataType dataType) {
        return dataType.getStateCount() != Integer.MAX_VALUE;
    }

} // class GeneralSubstitutionModel
