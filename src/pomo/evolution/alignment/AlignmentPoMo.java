/*
 * File Alignment.java
 *
 * Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
 *
 * This file is part of BEAST2.
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership and licensing.
 *
 * BEAST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *  BEAST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with BEAST; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
package pomo.evolution.alignment;


import beast.base.core.Description;
import beast.base.core.Log;
import beast.base.evolution.alignment.Alignment;
import beast.base.evolution.alignment.Sequence;
import beast.base.evolution.datatype.StandardData;
import pomo.evolution.datatype.DataTypePoMo;
import pomo.evolution.datatype.PoMoData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


@Description("Class representing PoMo alignment data")
public class AlignmentPoMo extends Alignment {

    // Use version.xml to declare data types
    static public void findDataTypesPoMo() {
    	String sDescription = "PoModata";
        if (!types.containsKey(sDescription)) {
            types.put(sDescription, new PoMoData());
        }
    }

//    public Input<List<SequencePoMo>> sequenceInput =
//            new Input<List<SequencePoMo>>("sequence", "sequence and meta data for particular taxon", new ArrayList<SequencePoMo>(), Validate.OPTIONAL);
//    public Input<Integer> stateCountInput = new Input<Integer>("statecount", "maximum number of states in all sequences");

    /**
     * state codes for the sequences *
     */
    protected List<List<Integer[]>> counts = new ArrayList<List<Integer[]>>();

    /**
     * data type, useful for converting String sequence to Code sequence, and back *
     */
    protected DataTypePoMo m_dataType;

    /**
     * pattern state encodings *
     */
    protected int [][][] sitePatterns; // #patters x #taxa

    /**
     * maps site nr to pattern nr *
     */
    protected int [] patternIndex;

    public AlignmentPoMo() {
    }

    /**
     * Constructor for testing purposes.
     *
     * @param sequences
     * @param stateCount
     * @param dataType
     * @throws Exception when validation fails
     */
    public AlignmentPoMo(List<SequencePoMo> sequences, Integer stateCount, DataTypePoMo dataType) throws Exception {

        for (SequencePoMo sequence : sequences) {
            sequenceInput.setValue(sequence, this);
        }
        initAndValidate();
    }


    @Override
    public void initAndValidate() throws IllegalArgumentException {
    	if (sequenceInput.get().size() == 0 && defaultInput.get().size() == 0) {
    		throw new IllegalArgumentException("Either a sequence input must be specified, or a map of strings must be specified");
    	}
    	
    	if (siteWeightsInput.get() != null) {
    		String sStr = siteWeightsInput.get().trim();
    		String [] strs = sStr.split(",");
    		siteWeights = new int[strs.length];
    		for (int i = 0; i< strs.length; i++) {
    			siteWeights[i] = Integer.parseInt(strs[i].trim());
    		}    		
    	}
    	
        //List<String> sDataTypes = new ArrayList<String>();
        //sDataTypes.add("PoModata");
        m_dataType=new PoMoData();
        //AddOnManager.find(beast.evolution.datatype.DataType.class, IMPLEMENTATION_DIR);
        //for (String sDataType : sDataTypes) {
        //    DataTypePoMo dataType = (DataTypePoMo) Class.forName(sDataType).newInstance();
        //    m_dataType = dataType;
        //}

        // grab data from child sequences
        taxaNames.clear();
        stateCounts.clear();
        counts.clear();
        if (sequenceInput.get().size() > 0) {
            for (Sequence seq : sequenceInput.get()) {
                // cast to SequencePoMo
                if (seq instanceof SequencePoMo sequencePoMo) {
                    //m_counts.add(seq.getSequence(getMap()));
                    //counts.add(seq.getSequence(m_dataType));
                    try {
                        counts.add(sequencePoMo.getSequencePoMo(m_dataType));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (taxaNames.indexOf(seq.taxonInput.get()) >= 0) {
                        throw new IllegalArgumentException("Duplicate taxon found in alignment: " + seq.taxonInput.get());
                    }
                    taxaNames.add(seq.taxonInput.get());
                    stateCounts.add(seq.totalCountInput.get());
                } else
                    throw new IllegalArgumentException("Require PoMo sequences ! " + seq.getClass().getName());
            }
            if (counts.size() == 0) {
                // no sequence data
                throw new IllegalArgumentException("Sequence data expected, but none found");
            }
        } else {
            for (String key : map.keySet()) {
                String sequence = map.get(key);
                List<Integer[]> list;
                try {
                    list = m_dataType.string2statePoMo(sequence);
                    counts.add(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (taxaNames.indexOf(key) >= 0) {
                    throw new IllegalArgumentException("Duplicate taxon found in alignment: " + key);
                }
                taxaNames.add(key);
                stateCounts.add(m_dataType.getStateCount());
            }
        }
        // Sanity check: make sure sequences are of same length
        int nLength = counts.get(0).size();
        if (!(m_dataType instanceof StandardData)) {
            for (List<Integer[]> seq : counts) {
                if (seq.size() != nLength) {
                    throw new IllegalArgumentException("Two sequences with different length found: " + nLength + " != " + seq.size());
                }
            }
        }
        if (siteWeights != null && siteWeights.length != nLength) {
            throw new RuntimeException("Number of weights (" + siteWeights.length + ") does not match sequence length (" + nLength +")");
        }

        calcPatterns();
        //Log.info.println(toString(false));
        //System.out.println("Ended log\n");
        setupAscertainment();
    }


    /**
     * Returns a List of Integer Lists where each Integer List represents
     * the sequence corresponding to a taxon.  The taxon is identified by
     * the position of the Integer List in the outer List, which corresponds
     * to the nodeNr of the corresponding leaf node and the position of the
     * taxon name in the taxaNames list. 
     *
     * @return integer representation of sequence alignment
     */
    public List<List<Integer[]>> getCountsPoMo() {
        return counts;
    }

    public DataTypePoMo getDataTypePoMo() {
        return m_dataType;
    }


    public int[][] getPatternPoMo(int iPattern) {
        return sitePatterns[iPattern];
    }

    public int[] getPatternPoMo(int iTaxon, int iPattern) {
        return sitePatterns[iPattern][iTaxon];
    }

    public int getPatternCount() {
        return sitePatterns.length;
    }


    /**
     * SiteComparator is used for ordering the sites,
     * which makes it easy to identify patterns.
     */
    class SiteComparatorPoMo implements Comparator<int[][]> {
        public int compare(int[][] o1, int[][] o2) {
            for (int i = 0; i < o1.length; i++) {
            	for (int i2 = 0; i2 < 4; i2++) {
	                if (o1[i][i2] > o2[i][i2]) {
	                    return 1;
	                }
	                if (o1[i][i2] < o2[i][i2]) {
	                    return -1;
	                }
            	}
            }
            return 0;
        }
    } // class SiteComparatorPoMo

    /**
     * calculate patterns from sequence data
     * *
     */
    protected void calcPatterns() {
        int nTaxa = counts.size();
        int nSites = counts.get(0).size();

        // convert data to transposed int array
        int[][][] nData = new int[nSites][nTaxa][4];
        for (int i = 0; i < nTaxa; i++) {
            List<Integer[]> sites = counts.get(i);
            for (int j = 0; j < nSites; j++) {
            	for (int j2 = 0; j2 < 4; j2++) {
            		nData[j][i][j2] = sites.get(j)[j2];
            	}
            }
        }

        // sort data
        SiteComparatorPoMo comparator = new SiteComparatorPoMo();
        Arrays.sort(nData, comparator);

        // count patterns in sorted data
        // if (siteWeights != null) the weights are recalculated below
        int nPatterns = 1;
        int[] weights = new int[nSites];
        weights[0] = 1;
        for (int i = 1; i < nSites; i++) {
            if (comparator.compare(nData[i - 1], nData[i]) != 0) {
                nPatterns++;
                nData[nPatterns - 1] = nData[i];
            }
            weights[nPatterns - 1]++;
        }
        System.out.println("found "+nPatterns+" patterns");

        // reserve memory for patterns
        patternWeight = new int[nPatterns];
        sitePatterns = new int[nPatterns][nTaxa][4];
        for (int i = 0; i < nPatterns; i++) {
            patternWeight[i] = weights[i];
            sitePatterns[i] = nData[i];
        }

        // find patterns for the sites
        patternIndex = new int[nSites];
        for (int i = 0; i < nSites; i++) {
            int[][] sites = new int[nTaxa][4];
            for (int j = 0; j < nTaxa; j++) {
            	for (int j2 = 0; j2 < 4; j2++) {
            		sites[j][j2] = counts.get(j).get(i)[j2];
            	}
            }
            patternIndex[i] = Arrays.binarySearch(sitePatterns, sites, comparator);
        }
        
        if (siteWeights != null) {
        	Arrays.fill(patternWeight, 0);
            for (int i = 0; i < nSites; i++) {
            	patternWeight[patternIndex[i]] += siteWeights[i];
            }        	
        }

        // determine maximum state count
        // Usually, the state count is equal for all sites,
        // though for SnAP analysis, this is typically not the case.
        maxStateCount = 0;
        for (int m_nStateCount1 : stateCounts) {
            maxStateCount = Math.max(maxStateCount, m_nStateCount1);
        }
        // report some statistics
        if( taxaNames.size() < 30 ) {
            for (int i = 0; i < taxaNames.size(); i++) {
                Log.info.println(taxaNames.get(i) + ": " + counts.get(i).size() + " " + stateCounts.get(i));
            }
        }

        if (stripInvariantSitesInput.get()) {
            // don't add patterns that are invariant, e.g. all gaps
            Log.info.println("Stripping invariant sites");

            int removedSites = 0;
            for (int i = 0; i < nPatterns; i++) {
                int[][] nPattern = sitePatterns[i];
                int[] iValue = nPattern[0];
                boolean bIsInvariant = true;
                for (int k = 1; k < nPattern.length; k++) {
                    if (nPattern[k] != iValue) {
                        bIsInvariant = false;
                        break;
                    }
                }
                if (bIsInvariant) {
                	removedSites += patternWeight[i]; 
                    patternWeight[i] = 0;

                    Log.info.print(" <" + iValue + "> ");
                }
            }
            Log.info.println(" removed " + removedSites + " sites ");
        }
    } // calcPatterns

    /**
     * @return the total weight of all the patterns (this is the effective number of sites)
     */
    private long getTotalWeight() {
        long totalWeight = 0;
        for (int weight : patternWeight) {
            totalWeight += weight;
        }
        return totalWeight;
    }


    /**
     * Pretty printing of vital statistics of an alignment including id, #taxa, #sites, #patterns and totalweight
     * @param singleLine true if the string should fit on one line
     * @return string representing this alignment
     */
    public String toString(boolean singleLine) {
        long totalWeight = getTotalWeight();
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getSimpleName()+ "(" + getID() + ")");

        if (singleLine) {
            builder.append(": [taxa, patterns, sites] = [" + getTaxonCount() + ", " + getPatternCount());
            builder.append(", " + getTotalWeight() + "]");
        } else {

            long siteCount = getSiteCount();

            builder.append('\n');
            builder.append("  " + getTaxonCount() + " taxa");
            builder.append('\n');
            builder.append("  " + siteCount + (siteCount == 1 ? " site": " sites") + (totalWeight == getSiteCount() ? "" : " with weight " + totalWeight + ""));
            builder.append('\n');
            if (siteCount > 1) {
                builder.append("  " + getPatternCount() + " patterns");
                builder.append('\n');
            }
        }
        return builder.toString();
    }

    /**
     * returns an array containing the non-ambiguous states that this state represents.
     */
    public boolean[] getStateSet(int iState) {
        return m_dataType.getStateSet(iState);
//        if (!isAmbiguousState(iState)) {
//            boolean[] stateSet = new boolean[m_nMaxStateCount];
//            stateSet[iState] = true;
//            return stateSet;
//        } else {
//        }
    }


} // class Data
