package pomo.evolution.datatype;

import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;

import beast.base.core.BEASTObject;
import beast.base.core.Description;
import beast.base.evolution.datatype.DataType;


public interface DataTypePoMo extends DataType {
    final static public char GAP_CHAR = '-';
    final static public char MISSING_CHAR = '?';

    /**
     * @return number of states for this data type.
     *         Assuming there is a finite number of states, or -1 otherwise.
     */
    int getStateCount();

    /**
     * Convert a sequence represented by a string into a sequence of integers
     * representing the state for this data type.
     * Ambiguous states should be represented by integer numbers higher than getStateCount()
     * throws exception when parsing error occur *
     */
    //List<Integer> string2state(String sSequence) throws Exception;
    List<Integer[]> string2statePoMo(String sSequence) throws Exception;

    /**
     * Convert an array of states into a sequence represented by a string.
     * This is the inverse of string2state()
     * throws exception when State cannot be mapped *
     */
    //String state2string(List<Integer> nStates) throws Exception;
    String state2stringPoMo(List<Integer[]> nStates) throws Exception;

    //String state2string(int[] nStates) throws Exception;
    String state2stringPoMo(int[][] nStates) throws Exception;

    /**
     * returns an array of length getStateCount() containing the (possibly ambiguous) states
     * that this state represents.
     */
    public boolean[] getStateSet(int iState);

    /**
     * returns an array with all non-ambiguous states represented by
     * a state.
     */
    public int[] getStatesForCode(int iState);

    boolean isAmbiguousState(int state);

    /**
     * true if the class is completely self contained and does not need any
     * further initialisation. Notable exception: GeneralDataype
     */
    boolean isStandard();

    /**
     * data type description, e.g. nucleotide, codon *
     */
    public String getTypeDescription();

    /**
     * Get character corresponding to a given state
     *
     * @param state state
     *              <p/>
     *              return corresponding character
     */
    public char getChar(int state);

    /**
     * Get a string code corresponding to a given state. By default this
     * calls getChar but overriding classes may return multicharacter codes.
     *
     * @param state state
     *              <p/>
     *              return corresponding code
     */
    public String getCode(int state);

    @Description(value = "Base class bringing class and interfaces together", isInheritable = false)
    public abstract class Base extends BEASTObject implements DataTypePoMo {
        /**
         * size of the state space *
         */
        int stateCount;

        /**
         * maps string encoding to state codes *
         */
        String codeMap;

        public String getCodeMap() {
            return codeMap;
        }

        /**
         * length of the encoding, e.g. 1 for nucleotide, 3 for codons *
         */
        int codeLength;

        /**
         * mapping codes to sets of states *
         */
        int[][] mapCodeToStateSet;

        @Override
        public void initAndValidate() throws IllegalArgumentException {
            if (mapCodeToStateSet != null) {
                if (mapCodeToStateSet.length != codeMap.length() / codeLength) {
                    throw new IllegalArgumentException("m_sCodeMap and m_mapCodeToStateSet have incompatible lengths");
                }
            }
        }

        @Override
        public int getStateCount() {
            return stateCount;
        }

        /**
         * implementation for single character per state encoding *
         */
        @Override
        public List<Integer[]> string2statePoMo(String data) throws Exception {
            List<Integer[]> sequence;
            sequence = new ArrayList<Integer[]>();
            // remove spaces
            data = data.replaceAll("\\s", "");
            //data = data.toUpperCase();
            // assume it is a comma separated string of integers
            String[] sStrs = data.split(",");
            for (String sStr : sStrs) {
            	String[] sStrs2 = sStr.split("-");
            	Integer[] nucs = new Integer[4];
            	int iN=0;
            	for (String sStr2 : sStrs2) {
            		try {
            			nucs[iN]=(Integer.parseInt(sStr2));
                		//sequence.add(Integer.parseInt(sStr));
                	} catch (NumberFormatException e) {
                		sequence.add(null);
                	}
            		iN++;
            	}
            	if (iN!=4) {
            		sequence.add(null);
            	}else{
            		sequence.add(nucs);
            	}
            }
            return sequence;
        } // string2statePoMo

        @Override
        public String state2stringPoMo(List<Integer[]> nrOfStates) {
            int[][] nrOfStates2 = new int[nrOfStates.size()][4];
            for (int i = 0; i < nrOfStates2.length; i++) {
            	for (int i2 = 0; i2 < 4; i2++) {
            		nrOfStates2[i][i2] = nrOfStates.get(i)[i2];
            	}
            }
            return state2stringPoMo(nrOfStates2);
        }

        /**
         * implementation for single character per state encoding *
         */
        @Override
        public String state2stringPoMo(int[][] nrOfStates) {
            StringBuffer buf = new StringBuffer();
            // produce a comma separated string of dash-separated quartets of integers
            for (int i = 0; i < nrOfStates.length - 1; i++) {
            	for (int i2 = 0; i2 < 3; i2++) {
            		buf.append(nrOfStates[i][i2] + "-");
            	}
            	buf.append(nrOfStates[i][3] + ",");
            }
            for (int i2 = 0; i2 < 3; i2++) {
        		buf.append(nrOfStates[nrOfStates.length - 1][i2] + "-");
        	}
        	buf.append(nrOfStates[nrOfStates.length - 1][3] + "");
            return buf.toString();
        } // state2string


        @Override
        public int[] getStatesForCode(int iState) {
            return mapCodeToStateSet[iState];
        }

        @Override
        public boolean[] getStateSet(int state) {
            boolean[] stateSet = new boolean[stateCount];
            int[] stateNumbers = getStatesForCode(state);
            for (int i : stateNumbers) {
                stateSet[i] = true;
            }
            return stateSet;
        } // getStateSet

        /** Default implementations represent non-ambiguous states as numbers
         * 0 ... stateCount-1, and ambiguous characters as numbers >= stateCount 
         * For data types that count something -- like microsattelites, or number 
         * of lineages in SNAPP -- a stateCount < 0 represents missing data. 
         */
        @Override
        public boolean isAmbiguousState(int state) {
            return (state < 0 || state >= stateCount);
        }

        @Override
        public boolean isStandard() {
            return true;
        }

        @Override
        public char getChar(int state) {
            return (char) (state + 'A');
        }

        @Override
        public String getCode(int state) {
            return String.valueOf(getChar(state));
        }

        @Override
        public String toString() {
            return getTypeDescription();
        }
        
        /** return state associated with a character */
        public Integer[] char2statePoMo(String character) throws Exception {
        	return string2statePoMo(character).get(0);
        }
    } // class Base

} // class DataType
