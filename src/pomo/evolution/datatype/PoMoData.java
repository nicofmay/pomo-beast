package pomo.evolution.datatype;

import beast.base.core.Description;

import java.util.List;


@Description("Datatype for PoMo sequences")
public class PoMoData extends DataTypePoMo.Base {
	
	public PoMoData() {
		stateCount = -1;
		mapCodeToStateSet = null;
		codeLength = -1;
		codeMap = null;
	}
	
	@Override
	public String getTypeDescription() {
		return "PoModata";
	}

	//******* required by DataType *******//

	@Override
	public String getCharacter(int code) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Integer> string2state(String sequence) throws IllegalArgumentException {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Integer> stringToEncoding(String sequence) throws IllegalArgumentException {
		throw new UnsupportedOperationException();
	}

	@Override
	public String state2string(List<Integer> encoding) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String encodingToString(List<Integer> encoding) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String state2string(int[] encoding) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String encodingToString(int[] encoding) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isAmbiguousCode(int code) {
		throw new UnsupportedOperationException();
	}
}