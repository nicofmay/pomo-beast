#takes DNA sequence with heterozygote positions representing a diploid individual, and returns two non-heterozygotic arbitrary phased haplotypes.

import argparse
import os

parser = argparse.ArgumentParser(description="takes DNA sequence with heterozygote positions representing a diploid individual, and returns two non-heterozygotic arbitrary phased haplotypes.")
parser.add_argument("-s","--sequence", help="DNA sequence to decouple")
parser.add_argument("-o", "--output", default="", help="output file")
args = parser.parse_args()

sequence=args.sequence
oFile=args.output

sequence1=sequence.replace("K","G").replace("M","A").replace("R","A").replace("Y","C").replace("S","C").replace("W","A").replace("k","g").replace("m","a").replace("r","a").replace("y","c").replace("s","c").replace("w","a")
sequence2=sequence.replace("K","T").replace("M","C").replace("R","G").replace("Y","T").replace("S","G").replace("W","T").replace("k","t").replace("m","c").replace("r","g").replace("y","t").replace("s","g").replace("w","t")

if oFile!="":
	f=open(oFile,"w")
	f.write(sequence1+"\n\n"+sequence2+"\n\n")
	f.close
else:
	print(sequence1+"\n")
	print(sequence2)
exit()
