PoMo
=============

This is a BEAST 2 package implementing an extension of PoMo, a substitution model that separates mutation and drift processes. It is useful for disentangling selection and mutation contributions to substitutions ( http://mbe.oxfordjournals.org/content/30/10/2249.full ) but most importantly to estimate populations or species trees while accounting for incomplete lineage sorting ( http://sysbio.oxfordjournals.org/content/64/6/1018 ).
Differently from *BEAST, it can deal with many genes and many samples without much additional computational cost. Its cost rather depends on number of species and site patterns.
Differently from SNAPP, it allows any level of divergence.
In this BEAST implementation, timed samples and sequencing errors are modeled, in addition to the features described in the links above.

To find information on how to write an input xml file, look at the pdf file in the /doc/ folder.


Creator: Nicola De Maio


License
-------

This software is free (as in freedom).  With the exception of the
libraries on which it depends, it is made available under the terms of
the GNU General Public Licence version 3, which is contained in this
directory in the file named COPYING.

This project has been supported by the Oxford Martin School.